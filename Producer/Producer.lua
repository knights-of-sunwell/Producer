--[[
Producer - a framework for World of Warcraft professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

Producer = LibStub( 'AceAddon-3.0'):NewAddon( 'Producer', 'AceEvent-3.0', 'AceConsole-3.0', 'AceHook-3.0', 'LibDebugLog-1.0')
if not Producer then return end -- already loaded and no upgrade necessary

local GetSpellBookItemName = GetSpellName or GetSpellBookItemName 

local PT3 = LibStub( 'LibPeriodicTable-3.1')
local L   = LibStub( 'AceLocale-3.0'):GetLocale( 'Producer')
local AceTimer = LibStub( 'AceTimer-3.0')

Producer.name, Producer.title = GetAddOnInfo( 'Producer')
Producer.version = format( '%s: %s |cFF20FF20"(WotLK)|r', L.Version, GetAddOnMetadata( 'Producer', 'Version') or 'unknown')

local selects = {}
local data    = { profession = {} }
local timer

do
	local ModulePrototype = {}

	function ModulePrototype:AddData( prof, name, data)
		if type(prof) ~= 'string' then error( format( "Usage: AddData( prof, name, data): 'prof' - string expected got '%s'.", type(prof)), 2) end
		if type(name) ~= 'string' then error( format( "Usage: AddData( prof, name, data): 'name' - string expected got '%s'.", type(name)), 2) end
		if type(data) ~= 'string' then error( format( "Usage: AddData( prof, name, data): 'data' - string expected got '%s'.", type(data)), 2) end
		local key = format( 'Producer.%s.%s.%s', self:GetName(), prof, name)
		PT3:AddData( key, data)
	end
	
	function ModulePrototype:RegisterSelection( prof, fkt)
		if type(prof) ~= 'string' then error( format( "Usage: RegisterSelection(prof, fkt): 'prof' - string expected got '%s'.", type(prof)), 2) end
		if type(fkt) ~= 'function' then error( format( "Usage: RegisterSelection(prof, fkt): 'fkt' - function expected got '%s'.", type(fkt)), 2) end
		local s = selects[prof] or {}
		if s[self:GetName()] == nil then
			s[self:GetName()] = fkt
			selects[prof] = s
		else
			error( format( "Usage: RegisterSelection(prof, fkt): Already registered '%s' for module %s.", prof, self:GetName()), 2)
		end
	end

	Producer:SetDefaultModulePrototype( ModulePrototype)
	Producer:SetDefaultModuleState( false)
end

local MAX_PROF_LEVEL   = 525
local MAX_PLAYER_LEVEL = 85
local DEFAULTS = { 
	profile = {
		xPos       = 400, 
		yPos        = 400,
		enabled     = true,
		debug       = false,
		locked      = false,
		autohide    = false,
		timeout     = 0,
		anchor      = 'BottomLeft',
		border      = 'Blizzard Tooltip',
		background  = 'Blizzard Tooltip',
		tabs        = false,
		search      = false,
		lock        = true,
		money       = true,
		icons       = true,
		craftTips   = true,
		reagentTips = true,
		sort        = 'difficulty', 
		smooth      = true, 
		canmake     = true, 
		favorite    = true, 
		orange      = true, 
		yellow      = true,
		level       = 0,
		trades = {
			['Alchemy']        = {},
			['Blacksmithing']  = {},
			['Cooking']        = {},
			['Enchanting']     = {},
			['Engineering']    = {},
			['First Aid']      = {},
			['Inscription']    = {},
			['Jewelcrafting']  = {},
			['Leatherworking'] = {},
			['Runeforge']      = {},
			['Smelting']       = {},
			['Tailoring']      = {},
		}
	},
	realm = {
		professions = {}
	},
	global = {
		favorites = {}
	}
}
local DIFFICULTY = {
	['optimal'] = 1,
	['medium']  = 2,
	['easy']    = 3,
	['trivial'] = 4,
}
local PROFESSIONS = {
	[2259]  = 'Alchemy',
--	[73979] = 'Archaeology',
	[2018]  = 'Blacksmithing',
	[2550]  = 'Cooking',
	[7411]  = 'Enchanting',
	[4036]  = 'Engineering',
	[3273]  = 'First Aid',
	[45357] = 'Inscription',
	[25229] = 'Jewelcrafting',
	[2108]  = 'Leatherworking',
	[2575]	= 'Mining',
	[53428] = 'Runeforge',
	[2656]  = 'Smelting',
	[3908]  = 'Tailoring',
}

function Producer:GetProfessions()
	return PROFESSIONS
end

function Producer:GetProfessionName( prof)
	for id,p in pairs( PROFESSIONS) do
		if p == prof then
			return GetSpellInfo( id)
		end
	end
	return prof
end

local function FindProfession( trade)
	for id,p in pairs( PROFESSIONS) do
		local name = GetSpellInfo( id)
		if name == trade then
			return p
		end
	end
end

local function GetTradeSkill()
	local trade, rank, maxRank = GetTradeSkillLine()
	local prof = FindProfession( trade) or ''
	Producer:Debug( 'GetTradeSkill():', trade, prof, rank, maxRank)
	if prof == 'Runeforge' then
		local level = UnitLevel( 'player') or MAX_PLAYER_LEVEL
		Producer:Debug( 'GetTradeSkill():', level)
		maxRank = math.floor( (MAX_PROF_LEVEL * MAX_PLAYER_LEVEL / level) + 0.5)
		rank = maxRank
	end
	if prof == 'Mining' then
		prof = 'Smelting'
		trade = Producer:GetProfessionName( prof)
	end
	return prof, trade, rank, maxRank
end

local function GetLinkID( link)
--	local id = link:match('item:(%d+)')
--	if id then return id end
--	id = link:match('enchant:(%d+)')
--	if id then return '-'..id end
--	id = link:match('spell:(%d+)')
--	if id then return '-'..id end
	local id = link and link:match('^|.-|.(.-:.-)[|:]')
	local ll = link and link:gsub( '|', '=')
	return id or ll or tostring(link)
end

local function ScanTrade()
	local num = GetNumTradeSkills()
	for i = 1,num do
		local name, kind, _, _, cast = GetTradeSkillInfo( i)
		if kind ~= 'header' and name then
			local trade = {}
			trade.difficulty = DIFFICULTY[kind]
			trade.index  = i
			trade.name   = name
			trade.cast   = cast
			trade.link   = GetTradeSkillItemLink( i)
			trade.recipe = GetTradeSkillRecipeLink( i)
			local minMade, maxMade = GetTradeSkillNumMade( i)
			trade.min = minMade or 1
			trade.max = maxMade or 1
			if not trade.link then
				Producer:Debug( 'missing link:', name)
			end
			if not trade.recipe then
				Producer:Debug( 'missing recipe:', name)
			end
			if trade.link == trade.recipe then
				Producer:Debug( 'link == recipe:', GetLinkID( trade.link), name)
			end
			trade.reagents = {}
			local numReags = GetTradeSkillNumReagents( i)
			for j = 1,numReags do
				local rName, rIcon, rNeed = GetTradeSkillReagentInfo( i, j)
				if rName and rIcon then
					local rLink = GetTradeSkillReagentItemLink( i, j)
					table.insert( trade.reagents, { name = rName, link = rLink, need = rNeed }) -- icon = rIcon, 
				end
			end
			trade.set   = L.Unsorted
			trade.skill = MAX_PROF_LEVEL
			data.profession[name] = trade
		end
	end
end

local function LoadSelection( prof, select)
	local s = selects[prof]
	if s == nil or s[select] == nil then
		Producer:Print( format( "Producer: Missing '%s' for module %s.", prof, select))
	elseif type(s[select]) == 'function' then
		s[select]()
		s[select] = true
	end
end

local missingSet = {}
local missingSkill = {}

local function ScanSet()
	LoadSelection( data.prof, data.select)
	local profGroup = 'Producer.' .. data.select .. '.' .. data.prof
	for _,t in pairs(data.profession) do
		local recipe = t.recipe
		local _, set = PT3:ItemInSet( recipe, profGroup)
		local grp = set and set:match( '([^%.]+)$') or L.Unsorted
--		Producer:Debug( 'ScanSet()', GetLinkID( recipe), set, grp, profGroup)
		t.set = grp
		if grp == L.Unsorted and not missingSet[recipe] then
			missingSet[recipe] = true
		end
	end
end

local function ScanSkill()
	local profRecipe = 'Producible.Recipe.' .. data.prof
	for _,t in pairs(data.profession) do
		local recipe = t.recipe
		local skill = PT3:ItemInSet( recipe, profRecipe)
--		Producer:Debug( 'ScanSkill()', GetLinkID( recipe), skill)
		skill = tonumber(skill)
		t.skill = skill or MAX_PROF_LEVEL
		if not skill and not missingSkill[recipe] then
			missingSkill[recipe] = true
		end
	end
end

local function UpdateData( prof, rank, maxRank)
	data.prof = prof
	data.rank = tonumber(rank) or MAX_PROF_LEVEL
	data.maxRank = tonumber(maxRank) or MAX_PROF_LEVEL
	data.title = format('%s |cFF7777FF(%i/%i)|r', Producer:GetProfessionName( prof), data.rank, data.maxRank)
	if data.tradeSkill then
		ScanTrade()
	end
	ScanSkill()
	ScanSet()
end

function Producer:UpdateFrame()
	if self.db.profile.enabled and not self:IsEnabled() then
		self:Enable()
	end
	if self:IsEnabled() and not self.db.profile.enabled then
		self:Disable()
	end
	if self:IsEnabled() and self.CraftFrame:IsVisible() then
		self.CraftFrame:Update( data, self.db.profile.xPos, self.db.profile.yPos, self.db.profile.locked)
	end
end

local function UpdateEvent()
--	Producer:Debug( 'UpdateEvent()')
	if data.tradeSkill then
		local prof, trade, rank, maxRank = GetTradeSkill()
		if prof and Producer:GetProfession( prof, 'enabled') then
			UpdateData( prof, rank, maxRank)
			Producer:UpdateFrame()
		end
	end
	timer = nil
end

local function StopSchedule()
	if timer then
		AceTimer:CancelTimer( timer, true)
		timer = nil
	end
end

local function UpdateSchedule()
	StopSchedule()
	if Producer.CraftFrame:IsVisible() then
		timer = AceTimer:ScheduleTimer( UpdateEvent, 0.1)
	end
end

function Producer:Open( prof, rank, maxRank)
	self:Debug( 'Producer:Open()')
	StopSchedule()
	data = {}
	data.profession = {}
	data.tradeSkill = true
	data.select = self:GetSelect( prof)
	data.isLink, data.player = IsTradeSkillLinked() 
	UpdateData( prof, rank, maxRank)
	if data.isLink then
		data.saved = false
	else
		data.player = UnitName( 'player')
		self:SaveChar( true)
	end
	self.CraftFrame:Show()
	self:UpdateFrame()
end

function Producer:UpdateDebug()
	self:ToggleDebugLog( self.db.profile.debug)
end

function Producer:InitConfig()
end

--[[---------------------------------------------------------------------------------
  Class Setup
------------------------------------------------------------------------------------]]
local function CorrectDB( db)
	for k,v in pairs(db.profile.trades) do
		if type(v) ~= 'table' then
			db.profile.trades[k] = {}
		end
	end
end

function Producer:OnInitialize()
	--register database events
	self.db = LibStub( 'AceDB-3.0'):New( 'ProducerDB', DEFAULTS, 'Default')
	CorrectDB( self.db)
	self:UpdateDebug()
--	table.insert( UISpecialFrames, 'Producer_CraftFrame')
	self:InitConfig()
	self.db.RegisterCallback( self, 'OnNewProfile')
	self.db.RegisterCallback( self, 'OnProfileChanged')
	self.db.RegisterCallback( self, 'OnProfileCopied')
	self.db.RegisterCallback( self, 'OnProfileReset')
	self.db.RegisterCallback( self, 'OnProfileDeleted')
end

function Producer:OnEnable()
	if IsAddOnLoaded( 'Blizzard_TradeSkillUI') then
		self:RawHook( 'TradeSkillFrame_Show', true)	-- RawHook
	else
		self:RegisterEvent( 'ADDON_LOADED')
	end
	self:RegisterEvent( 'TRADE_SKILL_SHOW')
	self:RegisterEvent( 'TRADE_SKILL_CLOSE')
	self:RegisterEvent( 'TRADE_SKILL_UPDATE')
	self:RegisterEvent( 'SPELLS_CHANGED')
	self:RegisterEvent( 'SKILL_LINES_CHANGED')
	self:RegisterEvent( 'BAG_UPDATE')
	self:RegisterEvent( 'UPDATE_TRADESKILL_RECAST')
	self:Debug( 'Producer enabled')
	self:Print( self.version, 'loaded')
end

function Producer:OnDisable()
	self:UnregisterEvent( 'TRADE_SKILL_SHOW')
	self:UnregisterEvent( 'TRADE_SKILL_CLOSE')
	self:UnregisterEvent( 'TRADE_SKILL_UPDATE')
	self:UnregisterEvent( 'SPELLS_CHANGED')
	self:UnregisterEvent( 'SKILL_LINES_CHANGED')
	self:UnregisterEvent( 'BAG_UPDATE')
	self:UnregisterEvent( 'UPDATE_TRADESKILL_RECAST')
	self:UnregisterEvent( 'ADDON_LOADED')
	self:Unhook( 'TradeSkillFrame_Show')
	self.CraftFrame:Hide()
	self:Debug( 'Producer disabled')
end

function Producer:ADDON_LOADED( event, addon)
--	self:Debug( 'Producer:ADDON_LOADED()', event, addon)
	if addon == 'Blizzard_TradeSkillUI' then
		self:RawHook( 'TradeSkillFrame_Show', true)	-- RawHook
		self:UnregisterEvent( 'ADDON_LOADED')
	end
end

function Producer:TRADE_SKILL_SHOW( event)
	local prof, trade, rank, maxRank = GetTradeSkill()
	self:Debug( 'Producer:TRADE_SKILL_SHOW()', prof, trade, rank, maxRank)
	if prof and self:GetProfession( prof, 'enabled') then
		TradeSkillFrame:SetScale(0.01)
        TradeSkillFrame:SetAttribute("UIPanelLayout-width", 0)
		self:Open( prof, rank, maxRank)
	elseif self:IsHooked( 'TradeSkillFrame_Show') then	
		self.CraftFrame:Hide()
		self.hooks['TradeSkillFrame_Show']()
	else
		self:Debug( 'Producer:TRADE_SKILL_SHOW()', 'missing hook for TradeSkillFrame_Show')
	end
end

function Producer:TRADE_SKILL_CLOSE()
	self:Debug( 'Producer:TRADE_SKILL_CLOSE()')
	self.CraftFrame:Hide()
end

function Producer:TRADE_SKILL_UPDATE()
	self:Debug( 'Producer:TRADE_SKILL_UPDATE()')
	UpdateSchedule()
end

function Producer:SPELLS_CHANGED()
	self:Debug( 'Producer:SPELLS_CHANGED()')
end

function Producer:SKILL_LINES_CHANGED()
	self:Debug( 'Producer:SKILL_LINES_CHANGED()')
	UpdateSchedule()
end

function Producer:BAG_UPDATE()
	self:Debug( 'Producer:BAG_UPDATE()')
	UpdateSchedule()
end

function Producer:UPDATE_TRADESKILL_RECAST()
	local repCount = GetTradeskillRepeatCount()
	if repCount > 1 then 
		data.repCount = repCount
	elseif data.repCount and data.repCount > 1 then
		data.repCount = data.repCount
	else
		data.repCount = nil
	end
	self:Debug( 'Producer:UPDATE_TRADESKILL_RECAST()', repCount, data.repCount)
	UpdateSchedule()
end

function Producer:TradeSkillFrame_Show()
	self:Debug( 'Producer:TradeSkillFrame_Show()')
end

--[[---------------------------------------------------------------------------------
  Main Methods
------------------------------------------------------------------------------------]]
local function Escape( s)
	return '"'..s:gsub( '"', '\\"')..'"'
end

local SerialSimple
local function SerialTable( arr)
	local res = {}
	for k,v in pairs(arr) do
		if type(k) == 'string' then
			table.insert( res, '[' .. Escape( k) .. ']=' .. SerialSimple( v))
		else
			table.insert( res, '[' .. tostring(k) .. ']=' .. SerialSimple( v))
		end
	end
	return '{' .. table.concat( res, ',') .. '}'
end

function SerialSimple( val)
	local t = type(val)
	if t == 'string'  then return Escape( val) end
	if t == 'number'  then return tostring( val) end
	if t == 'boolean' then return tostring( val) end
	if t == 'table'   then return SerialTable( val) end
	return 'nil'
end

local function SerialValue( val)
	return 'return ' .. SerialSimple( val)
end

local function UnserialValue( s)
	if type(s) == 'string' then
		local fkt, msg = loadstring( s , 'UnserialValue')
		if fkt then 
			return fkt() or {}
		else
			Producer:Print( msg)
		end
	end 
	return {}
end

function Producer:SaveChar( saved)
	data.saved = saved
	local professions = self.db.realm.professions[data.prof] or {}
	if saved then
--		professions[data.player] = data
		professions[data.player] = SerialValue( data)
	else
		professions[data.player] = nil
	end
	self.db.realm.professions[data.prof] = professions
end

function Producer:ChangeSelect( select)
	if self:IsSelection(data.prof, select) then
		data.select = select
		ScanSet()
		self:UpdateFrame()
	else
		self:Debug( 'Producer:ChangeSelect() missing select', select)
	end
end

function Producer:CastSpellOf( prof)
	local prof1, prof2, archaeology, fishing, cooking, firstAid = GetProfessions()
	if prof1 then
		local name, _, _, _, _, spelloffset = GetProfessionInfo( prof1)
		if prof == name then
			CastSpell( spelloffset + 1, 'spell')
			return
		end
	end
	if prof2 then
		local name, _, _, _, _, spelloffset = GetProfessionInfo( prof2)
		if prof == name then
			CastSpell( spelloffset + 1, 'spell')
			return
		end
	end
	if cooking then
		local name, _, _, _, _, spelloffset = GetProfessionInfo( cooking)
		if prof == name then
			CastSpell( spelloffset + 1, 'spell')
			return
		end
	end
	if firstAid then
		local name, _, _, _, _, spelloffset = GetProfessionInfo( firstAid)
		if prof == name then
			CastSpell( spelloffset + 1, 'spell')
			return
		end
	end
	self:Print( 'illegal CastSpell()', prof)
end

function Producer:SwitchToChar( prof, name)
--	Producer:Debug( 'SwitchToChar()', prof, name)
	TradeSkillFrame_Hide()
	CloseTradeSkill()
	local professions = self.db.realm.professions[prof]
	if professions then
		local userData = professions[name]
		if userData then
			if type(userData) == 'table' then
				data = userData
				professions[name] = SerialValue( userData)
			else
				data = UnserialValue( userData)
			end
--			data = userData
			if type(data.profession) ~= 'table' then
				data.profession = {}
			end
			data.tradeSkill = false
			data.select = self:GetSelect( data.prof)
			data.isLink = 1
			data.saved = true
			UpdateData( data.prof, data.rank, data.maxRank)
			self.CraftFrame:Show()
			self:UpdateFrame()
		else
			self.CraftFrame:Hide()
			self:Debug( 'Producer:SwitchToChar() missing player', name)
		end
	else
		self.CraftFrame:Hide()
		self:Debug( 'Producer:SwitchToChar() missing profession', prof)
	end
end

function Producer:SwitchCharOpen( grp)
	if self.CharFrame:IsVisible() then
		self.CharFrame:Hide()
	else
		self.CharFrame:Show( grp, self.db.realm.professions or {})
	end
end

function Producer:SwitchSelectOpen( grp)
	if self.SelectFrame:IsVisible() then
		self.SelectFrame:Hide()
	else
		self.SelectFrame:Show( grp, selects[data.prof] or {}, data.select)
	end
end

--[[---------------------------------------------------------------------------------
  Set/Get Methods
------------------------------------------------------------------------------------]]
function Producer:GetProfession( trade, prop)
	self:Debug( 'Producer:GetProfession', trade, prop)
	if trade then
		local t = self.db.profile.trades[trade]
		if t and t[prop] ~= nil then
			return t[prop]
		end
	end
	return self.db.profile[prop]
end

function Producer:SetProfession( trade, prop, value)
	self:Debug( 'Producer:SetProfession', trade, prop, value)
	if trade then
		local t = self.db.profile.trades[trade] or {}
		t[prop] = value
		self.db.profile.trades[trade] = t
	else
		for _,v in pairs(self.db.profile.trades) do
			v[prop] = nil
		end
		self.db.profile[prop] = value
	end
	self:UpdateFrame()
end

function Producer:IsSelection( trade, value)
	local p = selects[trade]
	return p and (p[value] ~= nil)
end

function Producer:GetSelections( trade)
	local p = selects[trade] or {}
	local s = {}
	for k in pairs(p) do
		s[k] = k
	end
	return s
end

function Producer:GetSelect( trade)
	local t = self.db.profile.trades[trade]
	return t and t.select or L.Default
end

function Producer:SetSelect( trade, value)
	if self:IsSelection( trade, value) then
		local t = self.db.profile.trades[trade] or {}
		t.select = value
		self.db.profile.trades[trade] = t
		self:UpdateFrame()
	end
end

--[[---------------------------------------------------------------------------------
Profile Functions
------------------------------------------------------------------------------------]]
function Producer:ListProfiles()
	self:Print( L.AvailableProfiles)
	local current = self.db:GetCurrentProfile()
	for _,name in ipairs(self.db:GetProfiles()) do
		self:Print( format( (name == current) and ' - |cFFFFFF00%s|r' or ' - %s', name))
	end
end

function Producer:OnNewProfile( event, db, name)
	self:UpdateFrame()
	self:Print( format( L.ProfileCreated, name))
end

function Producer:OnProfileChanged( event, db, name)
	self:UpdateFrame()
	self:Print( format( L.ProfileLoaded, name))
end

function Producer:OnProfileCopied( event, db, name)
	self:UpdateFrame()
	self:Print( format( L.ProfileCopied, name))
end

function Producer:OnProfileReset( event, db)
	self:UpdateFrame()
	self:Print( format( L.ProfileReset, db:GetCurrentProfile()))
end

function Producer:OnProfileDeleted( event, db, name)
	self:Print( format( L.ProfileDeleted, name))
end
