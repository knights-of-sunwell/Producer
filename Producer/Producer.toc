## Interface: 40000
## Title: Producer
## Notes: Retooled Professions
## Notes-ruRU: Изменяет окно профессий
## Notes-deDE: Neugestaltete Berufe
## Author: Grayal
## DefaultState: enabled
## SavedVariables: ProducerDB
## OptionalDeps: Ace3, LibDebugLog-1.0, LibPeriodicTable-3.1, LibSharedMedia-3.0, Snowflake-2.0, AckisRecipeList
## X-Embeds: Ace3, LibDebugLog-1.0, LibPeriodicTable-3.1, LibSharedMedia-3.0, Snowflake-2.0
## X-Email: grayal@gmx.de
## X-Category: Tradeskill
## X-CompatibleLocales: enUS, enGB, deDE, ruRU, zhCN
## Version: 2.5.9
## X-Curse-Packaged-Version: r194
## X-Curse-Project-Name: Producer
## X-Curse-Project-ID: producer
## X-Curse-Repository-ID: wow/producer/mainline

embeds.xml

Producible.lua
Localization.lua
Producer-Head.lua
Producer.lua
CraftFrame.lua
CharFrame.lua
SelectFrame.lua
Defaults.lua
Options.lua
