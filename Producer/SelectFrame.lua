--[[
Producer - a framework for World of Warcraft professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local Snowflake = LibStub( 'Snowflake-2.0')
local Producer = LibStub( 'AceAddon-3.0'):GetAddon( 'Producer')
local L = LibStub( 'AceLocale-3.0'):GetLocale( 'Producer')

local SelectFrame = Snowflake:CreateMain()
SelectFrame.frame:Hide()
Producer.SelectFrame = SelectFrame

local function HideFrame()
	SelectFrame.frame:Hide()
	SelectFrame:CloseNeighbour()
end

local function CreateList( selects, default, grp)
	grp:AddLine{ text = L.SwitchSelect, type = 'description' }
	grp:AddLine()
	for k in pairs(selects) do
		grp:AddLine{ text = k, action = 'ChangeSelect', params = { Producer, k }, closeWhenClicked = true, checked = (k == default) or nil }
	end
	grp:AddLine()
	grp:AddLine{ text = L.CloseMenu, action = HideFrame, params = {} }
end

function SelectFrame:Show( grp, selects, default)
	self.parent = grp
	self.frame:SetParent( grp.frame)
	self:SetCallback( 'OnClose', HideFrame)
	self:SetCallback( 'OnTimeOut', HideFrame)
	self:SetFunction( CreateList, { selects, default })
	self:Refresh()
	local dx = grp.frame:GetLeft() + grp.frame:GetRight() - GetScreenWidth()
	local p = (dx >= 0) and 'TOPRIGHT' or 'TOPLEFT'
	local r = (dx >= 0) and 'TOPLEFT' or 'TOPRIGHT'
	self.frame:ClearAllPoints()
	self.frame:SetPoint( p, grp.frame, r)
	self.frame:Show()
	self:StartTimer( 3.0)
end

function SelectFrame:Hide()
	HideFrame()
end

