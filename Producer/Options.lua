--[[
Producer - a framework for World of Warcraft professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local Producer = LibStub( 'AceAddon-3.0'):GetAddon( 'Producer')
local L = LibStub( 'AceLocale-3.0'):GetLocale( 'Producer')
local SM = LibStub('LibSharedMedia-3.0')
	
local SORT_TYPES = {
	['none']       = 1,
	['level']      = 2,
	['difficulty'] = 3,
	['alpha']      = 4,
}
local SORT_DESC = {
	L.SortNone,
	L.SortLevel,
	L.SortDifficulty,
	L.SortAlpha,
}
local ANCHOR_POS = {
	['TopLeft']     = L.TopLeft,
	['TopRight']    = L.TopRight,
	['BottomLeft']  = L.BottomLeft,
	['BottomRight'] = L.BottomRight,
}

local function GetProperty( info)
	return Producer.db.profile[info[#info]]
end

local function SetProperty( info, value)
	Producer.db.profile[info[#info]] = value
	Producer:UpdateFrame()
end

local function OnTimeout()
	return Producer.db.profile.timeout == 0
end

local function IsDebug()
	return Producer.db.profile.debug
end

local function SetDebug( info, value) 
	Producer.db.profile.debug = value
	Producer:UpdateDebug()
end

local function GetProfession( info)
	return Producer:GetProfession( info.arg, info[#info])
end

local function SetProfession( info, value)
	Producer:SetProfession( info.arg, info[#info], value)
end

local function GetNumber( info)
	return tostring(Producer:GetProfession( info.arg, info[#info]) or '0')
end

local function SetNumber( info, value)
	Producer:SetProfession( info.arg, info[#info], tonumber(value) or 0)
end

local function GetBorder( info)
	local b = Producer.db.profile.border
	local list = SM:List( 'border') or {}
	for k,v in pairs(list) do
		if v == b then
			return k
		end
	end
end

local function SetBorder( info, value)
	local b = SM:List( 'border')[value] or 'Blizzard Tooltip'
	Producer.db.profile.border = b
	Producer:UpdateFrame()
end

local function GetBackground( info)
	local b = Producer.db.profile.background
	local list = SM:List( 'background') or {}
	for k,v in pairs(list) do
		if v == b then
			return k
		end
	end
end

local function SetBackground( info, value)
	local b = SM:List( 'background')[value] or 'Blizzard Tooltip'
	Producer.db.profile.background = b
	Producer:UpdateFrame()
end

local function PrintVersion() 
	Producer:Print( Producer.version) 
end

local function PrintHelp()
	local function PrintCmd( cmd, desc) Producer:Print( format( ' - |cFF33FF99%s|r: %s', cmd, desc)) end
	Producer:Print( 'Commands (/prod, /producer)')
	PrintCmd( 'enable', L.EnabledDesc)
	PrintCmd( 'lock', L.LockDesc)
	PrintCmd( 'debug', L.DebugDesc)
	PrintCmd( 'version', L.VersionDesc)
end

local function GetSort( info)
	return SORT_TYPES[GetProfession( info)]
end

local function SetSort( info, value)
	value = tonumber(value)
	if value then
		for k,v in pairs(SORT_TYPES) do
			if v == value then
				SetProfession( info, k)
				break
			end
		end
	end
end

local function GetSelections( info)
	return Producer:GetSelections( info.arg)
end

local function GetSelect( info)
	return Producer:GetSelect( info.arg)
end

local function SetSelect( info, value)
	return Producer:SetSelect( info.arg, value)
end

local function GetName( info)
	local id = info.arg
	if id then
		local name = GetSpellInfo( id)
		return name
	end
	return 'ALL'
end

local function ProfessionTable( id, prof)
	local args = {
		name     = { type = 'description', order = 0, name = GetName,  arg = id, cmdHidden = true },
		space    = { type = 'description', order = 1, name = '',   cmdHidden = true },
		enabled  = { type = 'toggle', order =  2, name = L.EnabledName,  desc = L.EnabledDesc,  arg = prof, width = 'full' },
		orange   = { type = 'toggle', order =  3, name = L.OrangeName,   desc = L.OrangeDesc,   arg = prof, width = 'full' },
		yellow   = { type = 'toggle', order =  4, name = L.YellowName,   desc = L.YellowDesc,   arg = prof, width = 'full' },
		canmake  = { type = 'toggle', order =  5, name = L.CanMakeName,  desc = L.CanMakeDesc,  arg = prof, width = 'full' },
		favorite = { type = 'toggle', order =  6, name = L.FavoriteName, desc = L.FavoriteDesc, arg = prof, width = 'full' },
		smooth   = { type = 'toggle', order =  7, name = L.SmoothName,   desc = L.SmoothDesc,   arg = prof, width = 'full' },
		sort     = { type = 'select', order =  8, name = L.SortName,     desc = L.SortDesc,     arg = prof, values = SORT_DESC, get = GetSort,   set = SetSort },
		level    = { type = 'input',  order = 10, name = L.LevelName,    desc = L.LevelDesc,    arg = prof, pattern = '^%d*$',  get = GetNumber, set = SetNumber },
	}
	if prof then
		args.select = { type = 'select', order =  9, name = L.SelectName, desc = L.SelectDesc, arg = prof, values = GetSelections, get = GetSelect, set = SetSelect }
	end
	return { type = 'group', order = id and 20 or 10, name = GetName,  arg = id, get = GetProfession, set = SetProfession, args = args }
end

local function TradeTable( order)
	local args = {}
	args['profs'] = ProfessionTable( nil, nil)
	args['space'] = { type = 'group', order = 1,  name = '', cmdHidden = true, args = {} }
	for id,prof in pairs( Producer:GetProfessions()) do
		args['prof'..prof] = ProfessionTable( id, prof)
	end
	return { type = 'group', order = order, name = L.Trades, dialogHidden = true, args = args } -- dialogInline = true, 
end

local options = {
	type = 'group', name = Producer.title, get = GetProperty, set = SetProperty,
	args = {
		release     = { type = 'description', order = 2,  name = Producer.version, cmdHidden = true },
		description = { type = 'description', order = 3,  name = L.Description,    cmdHidden = true },
		
		space1   = { type = 'description', order = 4,  name = '', cmdHidden = true },
		enabled  = { type = 'toggle', order = 14, name = L.EnabledName,  desc = L.EnabledDesc },
		debug    = { type = 'toggle', order = 15, name = L.DebugName,    desc = L.DebugDesc,   get = IsDebug, set = SetDebug },
		locked   = { type = 'toggle', order = 16, name = L.LockedName,   desc = L.LockedDesc },
		anchor   = { type = 'select', order = 17, name = L.AnchorName,   desc = L.AnchorDesc, values = ANCHOR_POS },
		tabs     = { type = 'toggle', order = 18, name = L.TabsName,     desc = L.TabsDesc },
		search   = { type = 'toggle', order = 19, name = L.SearchName,   desc = L.SearchDesc },

		space3      = { type = 'description', order = 30,  name = '', cmdHidden = true },
		craftTips   = { type = 'toggle', order = 31, name = L.CraftTipsName,   desc = L.CraftTipsDesc },
		reagentTips = { type = 'toggle', order = 33, name = L.ReagentTipsName, desc = L.ReagentTipsDesc },
		icons       = { type = 'toggle', order = 32, name = L.IconName,        desc = L.IconDesc },
		money       = { type = 'toggle', order = 34, name = L.MoneyName,       desc = L.MoneyDesc },
		lock        = { type = 'toggle', order = 35, name = L.LockName,        desc = L.LockDesc },

		space4      = { type = 'description', order = 40,  name = '', cmdHidden = true },
		timeout     = { type = 'range',  order = 41, name = L.TimeoutName,    desc = L.TimeoutDesc,    min = 0, max = 60, step = 1 },
		autohide    = { type = 'toggle', order = 42, name = L.AutohideName,   desc = L.AutohideDesc,   disabled = OnTimeout },
		border      = { type = 'select', order = 44, name = L.BorderName,     desc = L.BorderDesc,     values = SM:List( 'border'),     get = GetBorder,     set = SetBorder },
		background  = { type = 'select', order = 45, name = L.BackgroundName, desc = L.BackgroundDesc, values = SM:List( 'background'), get = GetBackground, set = SetBackground },

		trades   = TradeTable( 50),
		
		version  = { type = 'toggle', order = 90, name = 'version', dialogHidden = true, disabled = true, get = PrintVersion, set = false },
		help     = { type = 'toggle', order = 91, name = 'help',    dialogHidden = true, disabled = true, get = PrintHelp,    set = false },
	},
}

local function OpenToCategory( appName, ...)
	local key = appName
	for n = 1,select( '#', ...) do
		key = key .. '\001' .. select( n, ...)
	end
	local grp = LibStub( 'AceConfigDialog-3.0').BlizOptions[appName]
	if grp and grp[key] then
		InterfaceOptionsFrame_OpenToCategory( grp[key].frame)
	end
end

local function OnCommand( input)
	if input and input:trim() ~= '' then
		LibStub( 'AceConfigCmd-3.0'):HandleCommand( 'producer', 'producer', input)
	else
		OpenToCategory( 'producer', 'profile')
		OpenToCategory( 'producer')
	end
end

function Producer:InitConfig()
	local profile = LibStub( 'AceDBOptions-3.0'):GetOptionsTable( self.db)
	options.args['profile'] = profile
	LibStub('AceConfig-3.0'):RegisterOptionsTable( 'producer', options)
	local dialog = LibStub( 'AceConfigDialog-3.0')
	dialog:AddToBlizOptions( 'producer', 'Producer')
	dialog:AddToBlizOptions( 'producer', L.Trades,  'Producer', 'trades')
	dialog:AddToBlizOptions( 'producer', L.Profile, 'Producer', 'profile')
	self:RegisterChatCommand( 'producer', OnCommand)
	self:RegisterChatCommand( 'prod', OnCommand)
end
