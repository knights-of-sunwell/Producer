--[[
Snowflake - a framework for World of Warcraft professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local AceGUI = LibStub( 'AceGUI-3.0')

local Type = 'SnowflakeEscape'
local Version = 2
	
local function OnAcquire(self)
end
	
local function OnRelease(self)
end

local function OnWidthSet( self, width)
	self.content:SetWidth(width)
	self.content.width = width
end

local function OnHeightSet( self, height)
	self.content:SetHeight(height)
	self.content.height = height
end

local function LayoutFinished(self, width, height)
	self:SetWidth( width or 100)
	self:SetHeight( height or 50)
end

local function Control_OnShow( self)
	self.obj:Fire( 'OnShow')
end

local function Control_OnHide( self)
	self.obj:Fire( 'OnHide')
end

local function Control_OnUpdate( self, elapsed)
	self.obj:Fire( 'OnUpdate', elapsed)
end

local function Constructor()
	local id  = AceGUI:GetNextWidgetNum( Type)
	local name = Type .. id
	local self = { num = id, type = Type }

	local frame = CreateFrame( 'Frame', name, UIParent)
	frame.obj = self
--	frame:SetBackdrop( LINE_BORDER)
--	frame:SetBackdropColor( 0, 0.5, 0)
	frame:SetWidth( 100)
	frame:SetHeight( 50)
	frame:SetToplevel( true)
	frame:EnableMouse( true)
	frame:SetFrameStrata('FULLSCREEN_DIALOG')
	frame:SetScript( 'OnShow',   Control_OnShow)
	frame:SetScript( 'OnHide',   Control_OnHide)
	frame:SetScript( 'OnUpdate', Control_OnUpdate)
	self.frame = frame
	
	local content = CreateFrame( 'Frame', nil, frame)
	content.obj = self
	content:SetAllPoints( frame)
	self.content = content
	
	self.OnAcquire      = OnAcquire
	self.OnRelease      = OnRelease
	self.OnWidthSet     = OnWidthSet
	self.OnHeightSet    = OnHeightSet
	self.LayoutFinished = LayoutFinished
	
	table.insert( UISpecialFrames, name)
	AceGUI:RegisterAsContainer( self)
	return self
end
	
AceGUI:RegisterWidgetType( Type, Constructor, Version)
