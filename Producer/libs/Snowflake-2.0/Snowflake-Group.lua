--[[
Snowflake - a library to provide a dropdown menu interface for professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local AceGUI = LibStub( 'AceGUI-3.0')
local AceTimer = LibStub( 'AceTimer-3.0')
local AceConsole = LibStub( 'AceConsole-3.0')
local Snowflake = LibStub( 'Snowflake-2.0')

--[[---------------------------------------------------------------------------------
  Menu
------------------------------------------------------------------------------------]]
local Type = 'SnowflakeGroup'
local Version = 2
local SLIDER_WIDTH = 8
local BORDER_WIDTH = 6

local PANE_BACKDROP  = {
	bgFile = 'Interface\\Tooltips\\UI-Tooltip-Background', tile = true, tileSize = 16,
	edgeFile = 'Interface\\Tooltips\\UI-Tooltip-Border', edgeSize = 16,
	insets = { left = 3, right = 3, top = 3, bottom = 3 },
}
	
local function OnAcquire( self)
	self.userdata.horizontal = 'RIGHT'
	self.userdata.vertical   = 'DOWN'
	self.userdata.scroll = 0
	self.userdata.backdrop = PANE_BACKDROP
end

local function OnRelease( self)
	self:StopTimer()
	self:SetLock( false)
	self:CloseNeighbour()
	if self.oldChildren then
		for _,b in pairs( self.oldChildren) do
			AceGUI:Release( b)
		end
		self.oldChildren = nil
	end
end

local function ShowSlider( self, visible)
	if visible then
		self.slider:Show()
	else
		self.slider:Hide()
	end
end

local function SetOffset( self, offset)
	self.content:ClearAllPoints()
	self.content:SetPoint( 'TOPLEFT', self.scroll, 'TOPLEFT', 0, offset)
	self.content:SetPoint( 'TOPRIGHT', self.scroll, 'TOPRIGHT', 0, offset)
end

local function LayoutFinished( self, width, height)
--	AceConsole:Print( 'LayoutFinished', self.frame:GetName(), width, height)
	width = math.max(width or 300, 50)
	height = math.max( height or 0, 20)
	self.content:SetHeight( height)
	self.content.height = height
	height = height + BORDER_WIDTH + BORDER_WIDTH
	width  = width + BORDER_WIDTH + SLIDER_WIDTH
	local screenHeight = GetScreenHeight()
	if height > screenHeight then
		self:SetHeight( screenHeight)
		ShowSlider(self, true)
		SetOffset( self, self.userdata.offset)
	else
		self:SetHeight( height)
		ShowSlider(self, false)
		SetOffset( self, 0)
	end
	self:SetWidth( width)
--	self:FixScroll()
end

--[[---------------------------------------------------------------------------------
  Scrollbar
------------------------------------------------------------------------------------]]
local function SetScroll( self, value)
	local height = self.scroll:GetHeight()
	local needed = self.content:GetHeight()
	if self.slider:IsVisible() and (needed > height) then
		self.userdata.offset = math.floor((needed - height) / 1000 * value)
	else
		self.userdata.offset = 0
	end
	SetOffset( self, self.userdata.offset)
	self.userdata.scroll = value
end

local function MoveScroll( self, value)
--	AceConsole:Print( 'MoveScroll: ', value)
	if self.slider:IsVisible() then
		local height = self.scroll:GetHeight()
		local needed = self.content:GetHeight()
		if needed > height then
			local diff = 25000 / (needed - height)
			if value < 0 then
				self.slider:SetValue( math.min( self.userdata.scroll + diff, 1000))
			else
				self.slider:SetValue( math.max( self.userdata.scroll - diff, 0))
			end
		end
	end
end

local function FixScroll( self)
	local height = self.scroll:GetHeight()
	local needed = self.content:GetHeight()
	if self.slider:IsVisible() and (needed > height) then
		local offset = self.userdata.offset or 0
		local diff = 1000 * offset / (needed - height)
		self.slider:SetValue( math.min( diff, 1000))
	else
		self.slider:SetValue( 0)
	end
end

local function Control_OnScrollValueChanged( frame, value)
	SetScroll( frame.obj, value)
end

local function Control_OnMouseWheel( frame, value)
	MoveScroll( frame.obj, value)
end

local function Control_OnSizeChanged( frame)
	FixScroll( frame.obj)
end

--[[---------------------------------------------------------------------------------
  Timeout
------------------------------------------------------------------------------------]]
local function OnTimeOut( self)
	self:Fire( 'OnTimeOut')
end

local function StopTimer( self)
	if self.userdata.timer then
		AceTimer:CancelTimer( self.userdata.timer, true)
		self.userdata.timer = nil
	end
end

local function ResetTimer( self)
	StopTimer( self)
	local timeout = self.userdata.timeout or 0
	if timeout > 0 then
		self.userdata.timer = AceTimer:ScheduleTimer( OnTimeOut, timeout, self)
	end
end

local function StartTimer( self, timeout)
	self.userdata.timeout = timeout
	ResetTimer( self)
end

--[[---------------------------------------------------------------------------------
  Neighbour
------------------------------------------------------------------------------------]]
local function CloseNeighbour( self)
	self.userdata.locked = false
	if self.neighbour then
		AceGUI:Release( self.neighbour)
		self.neighbour = nil
	end
end

local function SetNeighbour( self, neighbour)
	CloseNeighbour( self)
	if neighbour then
		self.neighbour = neighbour
		neighbour:SetUserData( 'main', self.userdata.main)
		neighbour.frame:SetParent( self.frame)
		neighbour.parent = self
	end
end

--[[---------------------------------------------------------------------------------
  Main
------------------------------------------------------------------------------------]]
local function AddLine( self, param)
	self:Fire( 'OnAddLine', param)
end

local function DockNeighbour( self, func, args)
	self:Fire( 'OnDock', func, args)
end

local function Neighbour( self, func, args)
	self:Fire( 'OnNeighbour', func, args)
end

local function GetTokenChild( self)
	local offset = 0 - (self.userdata.offset or 0)
	local key = self.userdata.key
	for _,b in pairs( self.children) do
		if b:GetUserData( 'key') == key then
			return b, offset
		end
		offset = offset + (b.frame:GetHeight() or 0)
	end
	self.userdata.key = nil
end

local function SetToken( self, key)
	if self.userdata.key ~= key then
		local b = GetTokenChild( self)
		if b and b.SetSelect then
			b:SetSelect( false)
		end
		if b and b.SetLock then
			b:SetLock( false)
		end
		self.userdata.key = key
	end
end

local function SetLock( self, lock)
	local locked = self:GetMainData( 'lockEnabled') and lock
	local grp = self.userdata.main
	while grp do
		grp:SetUserData( 'locked', locked)
		local b = GetTokenChild( grp)
		if b and b.SetLock then
			b:SetLock( locked)
		end
		if grp == self then
			locked = false
		end
		grp = grp.neighbour
	end
end

local function SetFunction( self, func, args)
	self.userdata.func = func
	self.userdata.args = args or {}
end

local function GetOldChild( self, key)
	local oldChildren = self.oldChildren or {}
	for i,b in pairs(oldChildren) do
		if b:GetUserData( 'key') == key then
			oldChildren[i] = nil
			return b
		end
	end
end

local function NextSpaceName( self)
	if self.userdata.space then
		self.userdata.space = self.userdata.space + 1
	else
		self.userdata.space = 1
	end
	return tostring(self.userdata.space)
end

local function errorhandler( err)
	return geterrorhandler()( err)
end

local function Call( self)
	local func = self.userdata.func
	if func then
		local temp = {}
		for _,v in ipairs(self.userdata.args) do 
			table.insert(temp, v)
		end
		table.insert(temp, self)
		local res, msg
		local this = self.userdata.args[1]
		if type(func) == 'function' then
			res, msg = xpcall( function() func( unpack(temp)) end, errorhandler)
		elseif type(this) == 'table' and type(this[func]) == 'function' then
			res, msg = xpcall( function() this[func]( unpack(temp)) end, errorhandler)
		else
			msg = 'not a function'
		end
		if res then 
			return msg
		else
			AceConsole:Print( 'Snowflake: Cannot call method:', func, msg)
		end
	end
end

local function Refresh( self)
	self.frame:SetBackdrop( self:GetMainData( 'backdrop') or PANE_BACKDROP)
	self.frame:SetBackdropColor( TOOLTIP_DEFAULT_BACKGROUND_COLOR.r, TOOLTIP_DEFAULT_BACKGROUND_COLOR.g, TOOLTIP_DEFAULT_BACKGROUND_COLOR.b)
	self:PauseLayout()
	self.userdata.space = 0
	self.oldChildren = self.children
	self.children = {}
	for _,b in pairs( self.oldChildren) do
		b:ClearAllPoints()
		b.frame:Hide()
	end
	Call( self)
	for _,b in pairs( self.oldChildren) do
		AceGUI:Release( b)
	end
	self.oldChildren = nil
	self:ResumeLayout()
	self:DoLayout()
	local last = GetTokenChild( self)
	if last and last.SetSelect then
		if self.neighbour and last:GetUserData( 'hasArrow') then
			last:SetSelect( true)
			DockNeighbour( self, last:GetUserData( 'func'), last:GetUserData( 'args'))
		else
			last:SetSelect( false)
			CloseNeighbour( self)
		end
	else
		CloseNeighbour( self)
	end
end

local function GetMinWidth( self)
	return self.frame:GetWidth()
end

local function GetMainData( self, key)
	local main = self:GetUserData( 'main')
	return main and main:GetUserData( key)
end

local function SetMainData( self, key, value)
	local main = self:GetUserData( 'main')
	if main then
		main:SetUserData( key, value)
	end
end

local function AddHelpTexture( frame)
	local tex = frame:CreateTexture( nil, 'OVERLAY')
	tex:SetBlendMode( 'ADD')
	tex:SetTexture( 1, 0, 0, 1)
	tex:SetAllPoints( frame)
end

local function Constructor()
	local id  = AceGUI:GetNextWidgetNum(Type)
	local self = { num = id, type = Type }

	local frame = CreateFrame( 'Frame', Type .. id, UIParent)
	frame.obj = self
	frame:SetBackdrop( PANE_BACKDROP)
	frame:SetBackdropColor( TOOLTIP_DEFAULT_BACKGROUND_COLOR.r, TOOLTIP_DEFAULT_BACKGROUND_COLOR.g, TOOLTIP_DEFAULT_BACKGROUND_COLOR.b)
--	frame:SetBackdropColor( 0, 0, 0.5)
	frame:SetWidth( 300)
	frame:SetHeight( 200)
	frame:SetAlpha( 1)
	frame:SetToplevel( true)
	frame:EnableMouse( true)
	frame:SetFrameStrata( 'FULLSCREEN_DIALOG')
	self.frame = frame
--	AddHelpTexture( frame)

	local scroll = CreateFrame( 'ScrollFrame', nil, frame)
	scroll.obj = self
	scroll:SetPoint( 'TOPLEFT', frame, 'TOPLEFT', BORDER_WIDTH, -BORDER_WIDTH)
	scroll:SetPoint( 'BOTTOMRIGHT', frame, 'BOTTOMRIGHT', -SLIDER_WIDTH, BORDER_WIDTH)
	scroll:EnableMouseWheel( true)
	scroll:SetScript( 'OnMouseWheel', Control_OnMouseWheel)
--	scroll:SetScript( 'OnSizeChanged', Control_OnSizeChanged)
	self.scroll = scroll

	local content = CreateFrame( 'Frame', nil, scroll)
	content.obj = self
	scroll:SetScrollChild( content)
	content:SetPoint( 'TOPLEFT', scroll, 'TOPLEFT', 0, 0)
	content:SetPoint( 'TOPRIGHT', scroll, 'TOPRIGHT', 0, 0)
	content:SetHeight( 20)
	self.content = content

	local slider = CreateFrame( 'Slider', nil, frame)
	slider.obj = self
	local bg = slider:CreateTexture( nil, 'BACKGROUND')
	bg:SetAllPoints( slider)
	bg:SetTexture( 0, 0, 0, 0.4)
	slider:SetOrientation( 'VERTICAL')
--	slider:SetHitRectInsets( 0, 0, -10, 0)
	slider:SetThumbTexture( 'Interface\\Buttons\\UI-SliderBar-Button-Vertical')
	slider:SetPoint( 'TOPRIGHT', frame, 'TOPRIGHT', 0, -BORDER_WIDTH)
	slider:SetPoint( 'BOTTOMRIGHT', frame, 'BOTTOMRIGHT', 0, BORDER_WIDTH)
	slider:SetMinMaxValues( 0, 1000)
	slider:SetValueStep( 1)
	slider:SetValue( 0)
	slider:SetWidth( SLIDER_WIDTH)
	slider:SetScript( 'OnValueChanged', Control_OnScrollValueChanged)
	slider:Hide()
	self.slider = slider

	self.OnAcquire      = OnAcquire
	self.OnRelease      = OnRelease
	self.LayoutFinished = LayoutFinished

	self.SetScroll  = SetScroll
	self.MoveScroll = MoveScroll
	self.FixScroll  = FixScroll

	self.StartTimer = StartTimer
	self.ResetTimer = ResetTimer
	self.StopTimer  = StopTimer

	self.SetNeighbour   = SetNeighbour
	self.CloseNeighbour = CloseNeighbour
	
	self.AddLine       = AddLine
	self.DockNeighbour = DockNeighbour
	self.Neighbour     = Neighbour
	self.GetTokenChild = GetTokenChild
	self.SetToken      = SetToken
	self.SetLock       = SetLock
	self.SetFunction   = SetFunction
	self.Refresh       = Refresh
	self.GetOldChild   = GetOldChild
	self.NextSpaceName = NextSpaceName
	self.GetMinWidth   = GetMinWidth
	self.GetMainData   = GetMainData
	self.SetMainData   = SetMainData

	AceGUI:RegisterAsContainer( self)
	return self
end

AceGUI:RegisterWidgetType( Type, Constructor, Version)
