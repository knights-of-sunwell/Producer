## Interface: 30300
## Title: Lib: Snowflake-2.0
## Notes: dropdown framework
## Author: Grayal
## X-Email: grayal@gmx.de
## X-Category: Library
## Version: 2.0
## X-Curse-Packaged-Version: r34
## X-Curse-Project-Name: LibSnowflake-2.0
## X-Curse-Project-ID: libsnowflake-2-0
## X-Curse-Repository-ID: wow/libsnowflake-2-0/mainline

lib.xml
