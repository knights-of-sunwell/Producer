--[[
Snowflake - a library to provide a dropdown menu interface for professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local MAJOR, MINOR = 'Snowflake-2.0', 2
local Snowflake = LibStub:NewLibrary( MAJOR, MINOR)
if not Snowflake then return end -- already loaded and no upgrade necessary

Snowflake.name = MAJOR
setmetatable( Snowflake, { __tostring = function() return MAJOR end })

local AceGUI = LibStub("AceGUI-3.0")
local AceConsole = LibStub( 'AceConsole-3.0')
local SHOW_HELP_TEXTURE = false

--[[---------------------------------------------------------------------------------
  Local Functions
------------------------------------------------------------------------------------]]
local function AddHelpTexture( frame)
	if SHOW_HELP_TEXTURE then
		local tex = frame:CreateTexture( nil, 'OVERLAY')
		tex:SetBlendMode( 'ADD')
		tex:SetTexture( random(), random(), random(), 1)
		tex:SetAllPoints( frame)
	end
end

local function SetTooltip( btn)
	local link = btn:GetUserData( 'link')
	if link then
		GameTooltip_SetDefaultAnchor( GameTooltip, btn.frame);
		GameTooltip:SetHyperlink( link)
	end
end

local function errorhandler( err)
	return geterrorhandler()( err)
end

local function Call( func, par, ...)
	if func then
		local temp = {}
		for _,v in ipairs(par) do
			table.insert(temp, v)
		end
		for i = 1,select( '#', ...) do 
			table.insert(temp, (select( i, ...)))
		end
		local res, msg
		local this = par[1]
		if type(func) == 'function' then
			res, msg = xpcall( function() func( unpack(temp)) end, errorhandler)
		elseif type(this) == 'table' and type(this[func]) == 'function' then
			res, msg = xpcall( function() this[func]( unpack(temp)) end, errorhandler)
		else
			msg = 'not a function'
		end
		if res then 
			return msg
		else
			AceConsole:Print( 'Snowflake: Cannot call method:', func, msg)
		end
	end
end

local function Button_OnEnter( btn)
--	AceConsole:Print( 'Button_OnEnter', btn.parent and btn.parent.frame:GetName(), btn.frame:GetName())
	local main = btn:GetUserData( 'main')
	if main then
		main:ResetTimer()
	end
	if not btn.parent then
		btn:SetSelect( false)
		AceConsole:Print( 'Snowflake: Button_OnEnter', btn.frame:GetName(), 'oh no, missing parent group')
	elseif not btn.parent:GetUserData( 'locked') then
		btn.parent:SetToken( btn:GetUserData( 'key'))
		btn:SetSelect( true)
		if btn:GetUserData( 'hasArrow') then
			btn.parent:Neighbour( btn:GetUserData( 'func'), btn:GetUserData( 'args'))
		else
			btn.parent:CloseNeighbour()
		end
	end
	SetTooltip( btn)
end

local function Button_OnLeave( btn)
	GameTooltip:Hide()
end

local function Button_OnClick( btn, name, button)
	if btn:GetUserData( 'hasArrow') then
		if btn.parent then
			btn.parent:SetToken( btn:GetUserData( 'key'))
			btn:SetSelect( true)
			btn.parent:SetLock( not btn.parent:GetUserData( 'locked'))
		else
			btn:SetLock( false)
		end
	elseif not btn:GetUserData( 'disabled') then
		Call( btn:GetUserData( 'action'), btn:GetUserData( 'params'), button)
	end
	local main = btn:GetUserData( 'main')
	if main then
		if btn:GetUserData( 'doClose') then
			main:Fire( 'OnClose')
		else
			main:Refresh()
		end
	end
end

local function Button_OnEnterPressed( btn, name, txt)
	if not btn:GetUserData( 'disabled') then
		Call( btn:GetUserData( 'action'), btn:GetUserData( 'params'), txt)
	end
	local main = btn:GetUserData( 'main')
	if main then
		if btn:GetUserData( 'doClose') then
			main:Fire( 'OnClose')
		else
			main:Refresh()
		end
	end
end

local function Button_OnTextChanged( btn, name, txt)
	if not btn:GetUserData( 'disabled') then
		btn:GetUserData( 'main'):Fire( 'OnTextChanged', btn, txt)
	end
end

local function Button_OnMouseDown( btn)
	local main = btn:GetUserData( 'main')
	if main then -- and (btn:GetUserData( 'disabled') or btn:GetUserData( 'hasArrow')) 
		main:Fire( 'OnMouseDown')
	end
end

local function Button_OnMouseUp( btn)
	local main = btn:GetUserData( 'main')
	if main then -- and (btn:GetUserData( 'disabled') or btn:GetUserData( 'hasArrow')) 
		main:Fire( 'OnMouseUp')
	end
end

local function InitImage( image, info)
	SetDesaturation( image, info.disabled)
	if info.checked then
		if info.icon then
			image:SetTexture( info.icon)
			image:SetWidth( info.iconWidth or 16)
			image:SetHeight( info.iconHeight or 16)
		else
			image:SetTexture( 'Interface\\Buttons\\UI-CheckBox-Check')
			image:SetWidth( 16)
			image:SetHeight( 16)
		end
		image:SetVertexColor( 1, 1, 1, 1)
		image:Show()
	elseif info.icon then
		image:SetTexture( info.icon)
		image:SetWidth( info.iconWidth or 16)
		image:SetHeight( info.iconHeight or 16)
		image:SetVertexColor( 1, 1, 1, 1)
		image:Show()
	else
		image:Hide()
	end
	if info.iconLeft and info.iconRight and info.iconTop and info.iconBottom then
		image:SetTexCoord( info.iconLeft, info.iconRight, info.iconTop, info.iconBottom)
	elseif info.icon and info.icon:find( '^Interface\\Icons\\') then
		image:SetTexCoord( 0.05, 0.95, 0.05, 0.95)
	else
		image:SetTexCoord( 0, 1, 0, 1)
	end
end

local function InitLabel( label, info)
	if info.type == 'header' then
		label:SetFontObject( GameFontNormalSmall)
	elseif info.disabled then
		label:SetFontObject( GameFontDisableSmall)
	else
		label:SetFontObject( GameFontHighlightSmall)
	end
	label:SetFont( STANDARD_TEXT_FONT, info.textHeight or UIDROPDOWNMENU_DEFAULT_TEXT_HEIGHT)
	label:SetJustifyH( info.justifyH or 'LEFT')
	label:SetText( info.text or '')
end

local function InitSecond( label, info)
	if info.type == 'header' then
		label:SetFontObject( GameFontNormalSmall)
	elseif info.disabled then
		label:SetFontObject( GameFontDisableSmall)
	else
		label:SetFontObject( GameFontHighlightSmall)
	end
	label:SetFont( STANDARD_TEXT_FONT, info.textHeight or UIDROPDOWNMENU_DEFAULT_TEXT_HEIGHT)
	label:SetJustifyH( info.justifyH2 or 'RIGHT')
	if info.second then
		label:SetText( info.second)
		label:Show()
	else
		label:SetText( '')
		label:Hide()
	end
end

local function Group_OnAddLine( grp, name, param)
	local info = param or { disabled = true }
	info.key = info.key or info.text or grp:NextSpaceName()
	local newType = 'SnowflakeButton'
	if info.type == 'input' then
		newType = 'SnowflakeEditBox'
	elseif info.type == 'description' then
		newType = 'SnowflakePlain'
	elseif info.type == 'header' then
		newType = 'SnowflakeTitle'
	elseif info.type then -- toggle, range, select, multiselect, color, keybinding
		AceConsole:Print( 'Group_OnAddLine type not implemented yet:', info.type)
	else
		info.type = 'execute'
	end
	local btn = grp:GetOldChild( info.key)
	if not btn then
		btn = AceGUI:Create( newType)
	elseif btn.type ~= newType then
		AceGUI:Release( btn)
		btn = AceGUI:Create( newType)
	end
	btn:SetUserData( 'main', grp:GetUserData( 'main'))
	btn.width = 'fill'
	btn:SetUserData( 'key', info.key or info.text or btn.frame:GetName())
	btn:SetUserData( 'link', info.link)
	if info.type == 'input' then
		btn:SetUserData( 'disabled', info.disabled)
		btn:SetUserData( 'func', info.func)
		btn:SetUserData( 'args', info.args or {})
		btn:SetUserData( 'action', info.action)
		btn:SetUserData( 'params', info.params or {})
		btn:SetUserData( 'doClose', info.closeWhenClicked)
		InitImage( btn.image, info)
		InitLabel( btn.label, info)
		if info.editNumber then
			btn.box:SetNumeric( true)
			btn.box:SetNumber( info.editNumber)
			btn.box:SetMaxLetters( info.maxLetters or 3)
		else
			btn.box:SetNumeric( false)
			btn.box:SetText( info.editText or '')
		end
		btn.box:SetWidth( info.editWidth or 40)
		btn.frame:SetHeight( btn.label:GetHeight() + 6)
		btn.frame:SetAlpha( info.alpha or 1)
		btn.highlight:Hide()
	elseif info.type == 'description' then
		btn:SetUserData( 'disabled', true)
		btn:SetUserData( 'maxWidth', info.maxWidth or 999999)
		InitImage( btn.image, info)
		InitLabel( btn.label, info)
		InitSecond( btn.second, info)
		btn.frame:SetHeight( btn.label:GetHeight() + 6)
		btn.frame:SetAlpha( info.alpha or 1)
	elseif info.type == 'header' then
		btn:SetUserData( 'disabled', info.disabled)
		btn:SetUserData( 'maxWidth', info.maxWidth or 999999)
		InitImage( btn.image, info)
		InitLabel( btn.label, info)
		btn.frame:SetHeight( btn.label:GetHeight() + 6)
	elseif info.type == 'execute' then
		btn:SetUserData( 'disabled', info.disabled)
		btn:SetUserData( 'func', info.func)
		btn:SetUserData( 'args', info.args or {})
		btn:SetUserData( 'action', info.action)
		btn:SetUserData( 'params', info.params or {})
		btn:SetUserData( 'hasArrow', info.hasArrow)
		btn:SetUserData( 'doClose', info.closeWhenClicked)
		btn:SetUserData( 'maxWidth', info.maxWidth or 999999)
		InitImage( btn.image, info)
		SetDesaturation( btn.arrow, info.disabled)
		btn.arrow:SetAlpha( info.hasArrow and 1 or 0)
		InitLabel( btn.label, info)
		InitSecond( btn.second, info)
		btn.frame:SetHeight( btn.label:GetHeight() + 6)
		btn.frame:SetAlpha( info.alpha or 1)
		btn.highlight:Hide()
	else
		AceConsole:Print( 'Group_OnAddLine unknown type:', info.type)
	end
	btn:SetCallback( 'OnEnter', Button_OnEnter)
	btn:SetCallback( 'OnLeave', Button_OnLeave)
	btn:SetCallback( 'OnClick', Button_OnClick)
	btn:SetCallback( 'OnMouseDown', Button_OnMouseDown)
	btn:SetCallback( 'OnMouseUp',   Button_OnMouseUp)
	btn:SetCallback( 'OnEnterPressed', Button_OnEnterPressed)
	btn:SetCallback( 'OnTextChanged',  Button_OnTextChanged)
	grp:AddChild( btn)
end

local function Dock( grp, btn, isBottom, isLeft, offset)
	grp:ClearAllPoints()
	if isBottom then
		if isLeft then
			grp:SetPoint( 'BOTTOMRIGHT', grp.parent.frame, 'BOTTOMLEFT',  0, offset or 0)
		else
			grp:SetPoint( 'BOTTOMLEFT', grp.parent.frame, 'BOTTOMRIGHT', 0, offset or 0)
		end
	else
		if isLeft then
			grp:SetPoint( 'TOPRIGHT', grp.parent.frame, 'TOPLEFT',  0, -offset or 0)
		else
			grp:SetPoint( 'TOPLEFT', grp.parent.frame, 'TOPRIGHT', 0, -offset or 0)
		end
	end
end

local function Group_OnDock( grp, name, func, args)
--	AceConsole:Print( 'Group_OnDock', grp.frame:GetName(), grp.neighbour.frame:GetName())
	local neighbour = grp.neighbour
	neighbour:SetFunction( func, args)
	neighbour:Refresh()
	local btn, offset = grp:GetTokenChild()
	Dock( neighbour, btn, false, false, offset)
	local frame = neighbour.frame
	local isBottom = (frame:GetTop() or 0) > GetScreenHeight()
	local isLeft   = (frame:GetRight() or 0) > GetScreenWidth()
	if isBottom or isLeft then
		Dock( neighbour, btn, isBottom, isLeft, offset)
	end
	local main = grp:GetUserData( 'main')
	local mainBottom = main and main.frame:GetBottom() or 0
	local bottom = frame:GetBottom()
	if bottom and bottom < mainBottom then
		local point, relativeTo, relativePoint, x, y = frame:GetPoint(1)
		frame:ClearAllPoints()
		frame:SetPoint(point, relativeTo, relativePoint, x or 0, (y or 0) - bottom + mainBottom)
	end
	local top = frame:GetTop()
	if top and top > GetScreenHeight() then
		local point, relativeTo, relativePoint, x, y = frame:GetPoint(1)
		frame:ClearAllPoints()
		frame:SetPoint( point, relativeTo, relativePoint, x or 0, (y or 0) - top + GetScreenHeight())
	end
end

local function Group_OnNeighbour( grp, name, func, args)
--	AceConsole:Print( 'Group_OnNeighbour', grp.frame:GetName())
	local neighbour = grp.neighbour
	if not neighbour then
		neighbour = AceGUI:Create( 'SnowflakeGroup')
		neighbour:SetLayout( 'ListZoom')
		neighbour:SetCallback( 'OnAddLine',   Group_OnAddLine)
		neighbour:SetCallback( 'OnNeighbour', Group_OnNeighbour)
		neighbour:SetCallback( 'OnDock',      Group_OnDock)
		grp:SetNeighbour( neighbour)
		AddHelpTexture( neighbour.frame)
	end
	grp:DockNeighbour( func, args)
	neighbour.frame:Show()
end

--[[---------------------------------------------------------------------------------
  Snowflake
------------------------------------------------------------------------------------]]
function Snowflake:CreateMain()
	local grp = AceGUI:Create( 'SnowflakeGroup')
	grp:SetUserData( 'main', grp)
	grp:SetCallback( 'OnAddLine',   Group_OnAddLine)
	grp:SetCallback( 'OnNeighbour', Group_OnNeighbour)
	grp:SetCallback( 'OnDock',      Group_OnDock)
	grp:SetLayout( 'ListZoom')
	AddHelpTexture( grp.frame)
	return grp
end

function Snowflake:SetBorder( grp, border, background)
	local backdrop = grp:GetMainData( 'backdrop')
	if backdrop then
		if border then
			backdrop.edgeFile = border
		end
		if background then
			backdrop.bgFile = background
		end
	end
end

--[[---------------------------------------------------------------------------------
  ListZoom
------------------------------------------------------------------------------------]]
AceGUI:RegisterLayout( 'ListZoom', function( content, children)
	local height = 0
	local width = 0
	for i,child in ipairs(children) do
		local frame = child.frame
		frame:ClearAllPoints()
		frame:Show()
		if i == 1 then
			frame:SetPoint( 'TOPLEFT', content, 'TOPLEFT', 0, 0)
		else
			frame:SetPoint( 'TOPLEFT', children[i-1].frame, 'BOTTOMLEFT', 0, 0)
		end
		if child.DoLayout then
			child:DoLayout()
		end
		if child.width == 'fill' then
			if child.GetMinWidth then
				width = math.max(width, child:GetMinWidth())
			end
			frame:SetPoint( 'RIGHT', content, 'RIGHT', 0, 0)
		end
		height = height + (frame.height or frame:GetHeight() or 0)
	end
	content.obj:LayoutFinished( width, height )
end)
