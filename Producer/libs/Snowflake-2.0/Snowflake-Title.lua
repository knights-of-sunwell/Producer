--[[
Snowflake - a library to provide a dropdown menu interface for professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local AceGUI = LibStub("AceGUI-3.0")
local Snowflake = LibStub( 'Snowflake-2.0')

--[[---------------------------------------------------------------------------------
  Title
------------------------------------------------------------------------------------]]
local Type = 'SnowflakeTitle'
local Version = 2

local function OnAcquire( self)
end

local function OnRelease( self)
end

local function Control_OnEnter( btn)
	btn.obj:Fire( 'OnEnter')
end

local function Control_OnLeave( btn)
	btn.obj:Fire( 'OnLeave')
end

local function Control_OnMouseDown( btn)
	btn.obj:Fire( 'OnMouseDown')
	AceGUI:ClearFocus()
end

local function Control_OnMouseUp( btn)
	btn.obj:Fire( 'OnMouseUp')
end

local function SetSelect( self, value)
end

local function GetMinWidth( self)
	local minWidth = self.minWidth + self.label:GetStringWidth()
	return math.min( minWidth, self.userdata.maxWidth)
end

local function GetMainData( self, key)
	local main = self.parent and self.parent:GetUserData( 'main')
	return main and main:GetUserData( key)
end

local function Constructor()
	local id  = AceGUI:GetNextWidgetNum( Type)
	local self = { num = id, type = Type }
	
	local frame = CreateFrame( 'Button', Type .. id, UIParent)
	frame.obj = self
	frame:SetHeight( 16)
	frame:SetAlpha( 1)
	frame:EnableMouse( true)
	frame:SetFrameStrata( 'FULLSCREEN_DIALOG')
	frame:SetScript( 'OnEnter', Control_OnEnter)
	frame:SetScript( 'OnLeave', Control_OnLeave)
	frame:SetScript( 'OnMouseDown', Control_OnMouseDown)
	frame:SetScript( 'OnMouseUp',   Control_OnMouseUp)
	self.frame = frame
	
	local image = frame:CreateTexture( nil, 'ARTWORK')
	image:SetTexture( 'Interface\\Buttons\\UI-CheckBox-Check')
	image:SetPoint( 'CENTER', frame, 'LEFT', 12, 0)
	image:SetWidth( 16)
	image:SetHeight( 16)
	image:Hide()
	self.image = image
	
	local label = frame:CreateFontString( nil, 'ARTWORK')
	label:SetFontObject( GameFontHighlightSmall)
	label:SetFont( STANDARD_TEXT_FONT, UIDROPDOWNMENU_DEFAULT_TEXT_HEIGHT)
	label:SetPoint( 'LEFT', frame, 'LEFT', 24, 0)
	label:SetPoint( 'RIGHT', frame, 'RIGHT', -4, 0)
	self.label = label
	
	self.minWidth = 24 + 4 -- [image.label_]
	
	self.OnAcquire = OnAcquire
	self.OnRelease = OnRelease
	
	self.SetSelect   = SetSelect
	self.GetMinWidth = GetMinWidth
	self.GetMainData = GetMainData
	
	AceGUI:RegisterAsWidget( self)
	return self
end

AceGUI:RegisterWidgetType( Type, Constructor, Version)
