--[[
Snowflake - a library to provide a dropdown menu interface for professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local AceGUI = LibStub("AceGUI-3.0")
local Snowflake = LibStub( 'Snowflake-2.0')

--[[---------------------------------------------------------------------------------
  EditBox
------------------------------------------------------------------------------------]]
local Type = 'SnowflakeEditBox'
local Version = 2

local function OnAcquire( self)
end

local function OnRelease( self)
	self:SetSelect( false)
end

local function Control_OnEnter( btn)
	btn.obj:Fire( 'OnEnter')
end

local function Control_OnLeave( btn)
	btn.obj:Fire( 'OnLeave')
end

local function Control_OnClick( btn)
	btn.obj:Fire( 'OnClick')
	btn.obj.box:ClearFocus()
end

local function Control_OnMouseDown( btn)
	btn.obj:Fire( 'OnMouseDown')
	AceGUI:ClearFocus()
end

local function Control_OnMouseUp( btn)
	btn.obj:Fire( 'OnMouseUp')
end

local function Control_OnEnterPressed( box)
	box.obj:Fire( 'OnEnterPressed', box:GetText())
	box:ClearFocus()
end

local function Control_OnEscapePressed( box)
	box:ClearFocus()
end

local function Control_OnEditFocusGained( box)
	box:HighlightText()
	AceGUI:ClearFocus()
end

local function Control_OnEditFocusLost( box)
	box:HighlightText( 0, 0)
end

local function Control_OnTextChanged( box)
	box.obj:Fire( 'OnTextChanged', box:GetText())
end

local function SetSelect( self, value)
	local hideSelect = self:GetMainData( 'hideSelect')
	if value and not self.userdata.disabled and not hideSelect then
		self.highlight:Show()
	else
		self.highlight:Hide()
	end
end

local function GetMinWidth( self)
	return self.minWidth + self.label:GetStringWidth() + self.box:GetWidth()
end

local function GetMainData( self, key)
	local main = self.parent and self.parent:GetUserData( 'main')
	return main and main:GetUserData( key)
end

local function Constructor()
	local id  = AceGUI:GetNextWidgetNum(Type)
	local self = { num = id, type = Type }
	
	local frame = CreateFrame( 'Button', Type .. id, UIParent)
	frame.obj = self
	frame:SetHeight( 16)
	frame:SetAlpha( 1)
	frame:SetFrameStrata( 'FULLSCREEN_DIALOG')
	frame:EnableMouse( true)
	frame:SetScript( 'OnEnter', Control_OnEnter)
	frame:SetScript( 'OnLeave', Control_OnLeave)
	frame:SetScript( 'OnClick', Control_OnClick)
	frame:SetScript( 'OnMouseDown', Control_OnMouseDown)
	frame:SetScript( 'OnMouseUp', Control_OnMouseUp)
	self.frame = frame
	
	local highlight = frame:CreateTexture( nil, 'BACKGROUND')
	highlight:SetTexture( 'Interface\\QuestFrame\\UI-QuestTitleHighlight')
	highlight:SetBlendMode( 'ADD')
	highlight:SetPoint( 'TOPLEFT', frame, 'TOPLEFT', 0, 0)
	highlight:SetPoint( 'TOPRIGHT', frame, 'TOPRIGHT', 0, 0)
	highlight:Hide()
	self.highlight = highlight
	
	local image = frame:CreateTexture( nil, 'ARTWORK')
	image:SetTexture( 'Interface\\Buttons\\UI-CheckBox-Check')
	image:SetPoint( 'CENTER', frame, 'LEFT', 12, 0)
	image:SetWidth( 16)
	image:SetHeight( 16)
	image:Hide()
	self.image = image
	
	local box = CreateFrame( 'EditBox', nil, frame)
	box.obj = self
	box:SetFontObject( ChatFontNormal)
	box:SetAutoFocus( false)
	box:SetWidth( 40)
	box:SetHeight( 20)
	box.back = box:CreateTexture( nil, 'BACKGROUND')
	box.back:SetTexture( 'Interface\\COMMON\\Common-CheckBox-Background')
	box.back:SetTexCoord( 0, 1, 0, 0.75)
	box.back:SetAllPoints( box)
	box:SetPoint( 'RIGHT', frame, 'RIGHT', -8, 0)
	box:SetScript( 'OnEnterPressed',    Control_OnEnterPressed)
	box:SetScript( 'OnEscapePressed',   Control_OnEscapePressed)
	box:SetScript( 'OnEditFocusGained', Control_OnEditFocusGained)
	box:SetScript( 'OnEditFocusLost',   Control_OnEditFocusLost)
	box:SetScript( 'OnTextChanged',     Control_OnTextChanged)
	self.box = box
	
	local label = frame:CreateFontString( nil, 'ARTWORK')
	label:SetFontObject( GameFontHighlightSmall)
	label:SetFont( STANDARD_TEXT_FONT, UIDROPDOWNMENU_DEFAULT_TEXT_HEIGHT)
	label:SetPoint( 'LEFT', frame, 'LEFT', 24, 0)
	label:SetPoint( 'RIGHT', box, 'LEFT', -4, 0)
	self.label = label

	self.minWidth = 24 + 4 + 8 -- [image.label_box__]
	
	self.OnAcquire  = OnAcquire
	self.OnRelease  = OnRelease

	self.SetSelect   = SetSelect
	self.GetMinWidth = GetMinWidth
	self.GetMainData = GetMainData
	
	AceGUI:RegisterAsWidget( self)
	return self
end

AceGUI:RegisterWidgetType( Type, Constructor, Version)
