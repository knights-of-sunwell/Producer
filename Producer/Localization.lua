--[[
Producer - a framework for World of Warcraft professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local AL = LibStub('AceLocale-3.0')

do
	local L = AL:NewLocale('Producer', 'enUS', true)
	L.AnchorDesc        = 'Select your anchor position for the main window.'
	L.AnchorName        = 'Anchor'
	L.ARL               = 'ARL'
	L.AutohideDesc      = 'After the selected timeout the main window and all subwindows will be closed.'
	L.AutohideName      = 'Autohide'
	L.AvailableProfiles = 'Available Profiles'
	L.BackgroundDesc    = 'Select your favorite background.'
	L.BackgroundName    = 'Background'
	L.Bank              = 'bank'
	L.BorderDesc        = 'Select your favorite border.'
	L.BorderName        = 'Border'
	L.BottomLeft        = 'BottomLeft'
	L.BottomRight       = 'BottomRight'
	L.CanMake           = 'can make'
	L.CanMakeDesc       = 'Show all recipes you can make.'
	L.CanMakeName       = 'Can Make'
	L.CannotCreate      = 'cannot create anything'
	L.Cast              = 'Cast'
	L.CloseMenu         = 'Close'
	L.Cooldown          = 'cool down'
	L.CraftTipsDesc     = 'Show tooltips for recipes.'
	L.CraftTipsName     = 'Craft Tooltips'
	L.CreateAll         = 'Create all'
	L.CreateNum         = 'Create more'
	L.CreateOne         = 'Create 1'
	L.DebugDesc         = 'Show debug messages. Only need for testing.'
	L.DebugName         = 'Debug'
	L.Default           = 'Default'
	L.DeleteChar        = 'DeleteChar'
	L.Description       = 'Producer is an Ace3 mod that overrides the Blizzard profession windows and replaces them with a popup menu with extra features.'
	L.EnabledDesc       = 'Activate the addon or profession.'
	L.EnabledName       = 'Enabled'
	L.Favorite          = 'Favorite'
	L.FavoriteDesc      = 'Show recipes you market as your favorites.'
	L.FavoriteName      = 'Favorite'
	L.Grouping          = 'Grouping'
	L.IconDesc          = 'Show icons for recipes.'
	L.IconName          = 'Show Icons'
	L.LevelDesc         = 'Show only recipes with a distance to your max skill less then this value.\n(recipe.skill >= max.skill - value)'
	L.LevelName         = 'Level distance'
	L.LinkRecipe        = 'LinkRecipe!'
	L.LinkResult        = 'LinkResult'
	L.LockedDesc        = 'Your selected submenu can be locked.'
	L.LockedName        = 'Button Locked'
	L.LockDesc          = 'Locked the position of the main window.'
	L.LockName          = 'Menu Locked'
	L.LowerSpells       = 'Show lower spells'
	L.MarketValue       = 'market value:'
	L.MatsCost          = 'Mats Cost'
	L.MoneyDesc         = 'Show the costs of the materials if it possible.'
	L.MoneyName         = 'Money Summary'
	L.OrangeDesc        = 'Show recipes with a actual orange state.'
	L.OrangeName        = 'Oranges'
	L.Oranges           = 'oranges recipe'
	L.PickupAll         = 'Pickup all'
	L.PickupNum         = 'Pickup more'
	L.PickupOne         = 'Pickup 1'
	L.Presorting        = 'Presorting'
	L.Profile           = 'Profile'
	L.ProfileCopied     = 'Copied settings from "%s"'
	L.ProfileCreated    = 'Created new profile "%s"'
	L.ProfileDeleted    = 'Deleted profile "%s"'
	L.ProfileLoaded     = 'Set profile to "%s"'
	L.ProfileReset      = 'Reset profile "%s"'
	L.ReagentTipsDesc   = 'Show tooltips for reagents.'
	L.ReagentTipsName   = 'Regent Tooltips'
	L.SaveChar          = 'SaveChar'
	L.Search            = 'Search'
	L.SearchDesc        = 'Show the search frame.'
	L.SearchName        = 'Show Search'
	L.SelectDesc        = 'Select a predefined sorting for your profession.'
	L.SelectName        = 'Selection'
	L.ShowALR           = 'Show Ackis Recipe List'
	L.SmoothDesc        = 'Show the skill level of the recipes with a smooth difficulty color.'
	L.SmoothName        = 'Smooth Difficulty Coloring'
	L.SortAlpha         = 'alphabetical'
	L.SortDesc          = 'Select the sort method for recipes.'
	L.SortDifficulty    = 'difficulty'
	L.SortLevel         = 'level'
	L.SortName          = 'Sorting'
	L.SortNone          = 'unsorted'
	L.SwitchChar        = 'Switch Character'
	L.SwitchCharDesc    = 'Show a saved profession of a other player.'
	L.SwitchSelect      = 'SwitchSelect'
	L.TabsDesc          = 'Show a bar with all profession for quick switch.'
	L.TabsName          = 'Show Tabs'
	L.TimeoutDesc       = 'Close the submenus after a short time.'
	L.TimeoutName       = 'Timeout'
	L.Trades            = 'Professions'
	L.TopLeft           = 'TopLeft'
	L.TopRight          = 'TopRight'
	L.Unknown           = 'unknown'
	L.Unsorted          = 'unsorted'
	L.Vendor            = 'vendor'
	L.VendorValue       = 'vendor value:'
	L.Version           = 'Version'
	L.VersionDesc       = 'Prints the current version'
	L.YellowDesc        = 'Show recipes with a actual yellow state.'
	L.YellowName        = 'Yellows'
	L.Yellows           = 'yellows recipe'
	L.Fast 				= "fast"
	L.FastDesc			= "first bag slot, |cff11ffffno popup|r"
end
do
	local L = AL:NewLocale('Producer', 'deDE')
	if L then 
		L.AnchorDesc        = 'Wähle deine Ankerpostion für das Hauptfenster'
		L.AnchorName        = 'Anker'
		L.AutohideDesc      = 'Nach dem gewähltem Timeout wird das Hauptfenster und alle Unterfenster geschlossen.'
		L.AutohideName      = 'Autoschließen'
		L.AvailableProfiles = 'Verfügbare Profile'
		L.BackgroundDesc    = 'Wähle deinen Wunschhintergrund.'
		L.BackgroundName    = 'Hintergrund'
		L.Bank              = 'Bank'
		L.BorderDesc        = 'Wähle deinen Wunschumrandung.'
		L.BorderName        = 'Rand'
		L.BottomLeft        = 'UntenLinks'
		L.BottomRight       = 'UntenRechts'
		L.CanMake           = 'herstellbare Gegenstände'
		L.CanMakeDesc       = 'Zeige alle Rezepte, die du herstellen kannst.'
		L.CanMakeName       = 'herstellbare Gegenstände'
		L.CannotCreate      = 'nichts herstellbar'
		L.Cast              = 'Verzaubern'
		L.CloseMenu         = 'Schließen'
		L.Cooldown          = 'Restzeit'
		L.CraftTipsDesc     = 'Zeige Tooltips für Rezepte an.'
		L.CraftTipsName     = 'Rezept Tooltips'
		L.CreateAll         = 'Alle Erstellen'
		L.CreateNum         = 'Mehrere Erstellen'
		L.CreateOne         = '1 Erstellen'
		L.DebugDesc         = 'Zeige Debug Nachrichten an. Wird nur zum Testen gebraucht.'
		L.DebugName         = 'Debug'
--		L.Default           = 'Vorgabe'
		L.DeleteChar        = 'Charakter löschen'
		L.Description       = 'Producer ist ein Ace3 Addon, welches das Blizzard Berufsfenster übersteuert und durch eine Popupmenü mit Zusatzfunktionen ersetzt.'
		L.EnabledDesc       = 'Aktiviert das Addon oder den Beruf.'
		L.EnabledName       = 'Aktivieren'
		L.Favorite          = 'Favorit'
		L.FavoriteDesc      = 'Zeige Rezepte an, die du als deine Favoriten markiert hast.'
		L.FavoriteName      = 'Favoriten'
		L.Grouping          = 'Gruppierung'
		L.IconDesc          = 'Zeige Symbole zu den Rezepte an.'
		L.IconName          = 'Symbole anzeigen'
		L.LevelDesc         = 'Zeigt nur Rezepte mit einem Abstand zu deinem maximalen Skill an, der kleiner als dieser Wert ist.\n(Rezept.Skill >= Max.Skill - Wert)'
		L.LevelName         = 'Levelabstand'
		L.LinkRecipe        = 'Rezept linken!'
		L.LinkResult        = 'Ergebnis linken'
		L.LockedDesc        = 'Deine ausgählten Untermenüs können verriegelt werden.'
		L.LockedName        = 'Buttons verriegeln'
		L.LockDesc          = 'Verriegelt die Position des Hauptfensters.'
		L.LockName          = 'Menü verriegeln'
		L.LowerSpells       = 'Zeige geringere Sprüche'
		L.MarketValue       = 'Marktpreis:'
		L.MatsCost          = 'Materialien Kosten'
		L.MoneyDesc         = 'Zeigt die Kosten der Materialien an, wenn dies möglich ist.'
		L.MoneyName         = 'Herstellungskosten'
		L.OrangeDesc        = 'Zeigt Rezepte mit dem Status Orange an.'
		L.OrangeName        = 'Orangefarbene Rezepte'
		L.Oranges           = 'Orangefarbene Rezepte'
		L.Presorting        = 'Vorsortierung'
		L.PickupAll         = 'Alle Entnehmen'
		L.PickupNum         = 'Mehrere Entnehmen'
		L.PickupOne         = '1 Entnehmen'
		L.Profile           = 'Profil'
		L.ProfileCopied     = 'Kopiere Werte von "%s"'
		L.ProfileCreated    = 'Erstellt ein neues Profil "%s"'
		L.ProfileDeleted    = 'Lösche Profil "%s"'
		L.ProfileLoaded     = 'Setze Profil auf "%s"'
		L.ProfileReset      = 'Rücksetzen Profil "%s"'
		L.ReagentTipsDesc   = 'Zeige Tooltips für Reagenzien an.'
		L.ReagentTipsName   = 'Reagenzien Tooltips'
		L.SaveChar          = 'Charakter speichern'
		L.Search            = 'Suche'
		L.SearchDesc        = 'Zeige den Suchrahmen an.'
		L.SearchName        = 'Zeige Suche'
		L.SelectDesc        = 'Wähle eine vordefinierte Sortierung für denen Beruf aus.'
		L.SelectName        = 'Auswahl'
		L.ShowALR           = 'Zeige Ackis Recipe List'
		L.SmoothDesc        = 'Zeige den Skilllevel der Rezepte mit einem weichen Farbübergang an.'
		L.SmoothName        = 'weiche Farbübergänge'
		L.SortAlpha         = 'alphabetisch'
		L.SortDesc          = 'Wähle die Art der Sortierung der Rezepte.'
		L.SortDifficulty    = 'nach Schwierigkeit'
		L.SortLevel         = 'nach Level'
		L.SortName          = 'Sortierung'
		L.SortNone          = 'unsortiert'
		L.SwitchChar        = 'Charakter wechseln'
		L.SwitchCharDesc    = 'Zeige einen gespeicherten Beruf eines anderen Spielers.'
		L.SwitchSelect      = 'Sortierung wechseln'
		L.TabsDesc          = 'Yeige eine Leiste mit allen Berufen zum schnellen Wechseln.'
		L.TabsName          = 'Zeige Tabs'
		L.TimeoutDesc       = 'Schließt die Untermenüs nach einer kurzen Zeit.'
		L.TimeoutName       = 'Abschaltzeit'
		L.Trades            = 'Berufe'
		L.TopLeft           = 'ObenLinks'
		L.TopRight          = 'ObenRechts'
		L.Unknown           = 'unbekannt'
		L.Unsorted          = 'unsortiert'
		L.Vendor            = 'Händler'
		L.VendorValue       = 'Händlerpreis:'
		L.Version           = 'Version'
		L.VersionDesc       = 'Druckt die aktuelle Version aus'
		L.YellowDesc        = 'Zeigt Rezepte mit dem Status Gelb an.'
		L.YellowName        = 'Gelbe Rezepte'
		L.Yellows           = 'Gelbe Rezepte'
	end
end

do
	local L = AL:NewLocale('Producer', 'frFR') -- I need help on this translation
	if L then 
		-- Translated by Pettigrow, roumf
		L["AnchorDesc"] = "Sélectionnez la position de l'ancre de la fenêtre principale."
		L["AnchorName"] = "Ancre"
		L["ARL"] = "ARL"
		L["AutohideDesc"] = "Une fois le délai d'attente dépassé, la fenêtre principale ainsi que tous les sous-menus seront fermés."
		L["AutohideName"] = "Masquer auto."
		L["AvailableProfiles"] = "Profils disponibles"
		L["BackgroundDesc"] = "Sélectionnez votre arrière-plan favori."
		L["BackgroundName"] = "Arrière-plan"
		L["Bank"] = "banque"
		L["BorderDesc"] = "Sélectionnez votre bordure favorite."
		L["BorderName"] = "Bordure"
		L["BottomLeft"] = "En bas à gauche"
		L["BottomRight"] = "En bas à droite"
		L["CanMake"] = "peut fabriquer"
		L["CanMakeDesc"] = "Affiche toutes les recettes que vous pouvez fabriquer."
		L["CanMakeName"] = "Peut fabriquer"
		L["CannotCreate"] = "impossible de créer quoique ce soit"
		L["Cast"] = "Lancé"
		L["CloseMenu"] = "Fermer"
		L["Cooldown"] = "recharge"
		L["CraftTipsDesc"] = "Affiche les bulles d'aide des recettes."
		L["CraftTipsName"] = "Bulles des recettes"
		L["CreateAll"] = "Tout créer"
		L["CreateNum"] = "Créer plus"
		L["CreateOne"] = "Créer 1"
		L["DebugDesc"] = "Affiche les messages de débogage. Utile uniquement à des fins de test."
		L["DebugName"] = "Débogage"
		L["Default"] = "Défaut"
		L["DeleteChar"] = "Effacer personnage"
		L["Description"] = "Producer est un addon Ace3 qui remplace les fenêtres de métier de Blizzard avec un menu popup proposant plus de fonctionnalités."
		L["EnabledDesc"] = "Active l'addon."
		L["EnabledName"] = "Activé"
		L["Favorite"] = "Favori"
		L["FavoriteDesc"] = "Affiche les recettes que vous avez marqués comme étant vos favorites."
		L["FavoriteName"] = "Favori"
		L["Grouping"] = "Regroupement"
		L["IconDesc"] = "Affiche les icônes des recettes."
		L["IconName"] = "Afficher les icônes"
		L["LevelDesc"] = "Affiche uniquement les recettes ayant une distance de compétence par rapport à votre compétence maximale inférieure à cette valeur.\n(recette.compétence >= max.compétence - valeur)"
		L["LevelName"] = "Distance de niveau"
		L["LinkRecipe"] = "Lien de la recette"
		L["LinkResult"] = "Lien du résultat"
		L["LockDesc"] = "Verrouille la position de la fenêtre principale."
		L["LockedDesc"] = "Votre sous-menu sélectionné peut être verrouillé."
		L["LockedName"] = "Verrouiller la fenêtre de métier"
		L["LockName"] = "Menu verrouillé"
		L["LowerSpells"] = "Afficher sorts de rangs inférieurs"
		L["MarketValue"] = "valeur du marché :"
		L["MatsCost"] = "Cout des matériaux"
		L["MoneyDesc"] = "Affiche le coût des matériaux si possible."
		L["MoneyName"] = "Cout total matériaux"
		L["OrangeDesc"] = "Affiche les recettes ayant une difficulté de type orange."
		L["OrangeName"] = "Oranges"
		L["Oranges"] = "recettes oranges"
		L["PickupAll"] = "Tout ramasser"
		L["PickupNum"] = "En ramasser d'avantage"
		L["PickupOne"] = "En ramasser un"
		L["Presorting"] = "Pré-tri"
		L["Profile"] = "Profil"
		L["ProfileCopied"] = "Paramètres copiés à partir de \"%s\"."
		L["ProfileCreated"] = "Nouveau profil \"%s\" créé."
		L["ProfileDeleted"] = "Profil \"%s\" supprimé"
		L["ProfileLoaded"] = "Profil définit à \"%s\"."
		L["ProfileReset"] = "Profil \"%s\" réinitialisé."
		L["ReagentTipsDesc"] = "Affiche les bulles d'aide des composants."
		L["ReagentTipsName"] = "Bulles des compos"
		L["SaveChar"] = "Sauvegarder personnage"
		L["Search"] = "Rechercher"
		L["SearchDesc"] = "Affiche le cadre de recherche."
		L["SearchName"] = "Afficher la recherche"
		L["SelectDesc"] = "Sélectionnez un tri prédéfini pour votre métier."
		L["SelectName"] = "Selection"
		L["ShowALR"] = "Afficher Ackis Recipe List"
		L["SmoothDesc"] = "Affiche le niveau des recettes avec une couleur de difficulté graduée."
		L["SmoothName"] = "Coloration graduée de la difficulté"
		L["SortAlpha"] = "alphabétique"
		L["SortDesc"] = "Sélectionnez la méthode de tri des recettes."
		L["SortDifficulty"] = "difficulté"
		L["SortLevel"] = "niveau"
		L["SortName"] = "Tri"
		L["SortNone"] = "non trié"
		L["SwitchChar"] = "Changer de personnage"
		L["SwitchCharDesc"] = "Affiche un métier sauvegardé d'un autre joueur."
		L["SwitchSelect"] = "Mode d'affichage"
		L["TabsDesc"] = "Affiche une barre contenant toutes les professions pour passer plus rapidement de l'un à l'autre."
		L["TabsName"] = "Afficher les onglets"
		L["TimeoutDesc"] = "Ferme les sous-menus après un court moment."
		L["TimeoutName"] = "Délai d'attente"
		L["TopLeft"] = "En haut à gauche"
		L["TopRight"] = "en haut à droite"
		L["Trades"] = "Métiers"
		L["Unknown"] = "inconnu"
		L["Unsorted"] = "non trié"
		L["Vendor"] = "vendeur"
		L["VendorValue"] = "valeur du marchand :"
		L["Version"] = "Version"
		L["VersionDesc"] = "Affiche la version actuelle."
		L["YellowDesc"] = "Affiche les recettes ayant une difficulté de type jaune."
		L["YellowName"] = "Jaune"
		L["Yellows"] = "recettes jaune"
	end
end

do
	local L = AL:NewLocale('Producer', 'esES') -- I need help on this translation
	if L then 
	end
end

do
	local L = AL:NewLocale('Producer', 'esMX') -- I need help on this translation
	if L then 
	end
end

do
	local L = AL:NewLocale('Producer', 'ruRU') -- I need help on this translation
	if L then 
		-- Translated by StingerSoft
--		L["AnchorDesc"] = "Описание: Якорь" -- Needs review
		L["AnchorName"] = "Якорь"
		L["ARL"] = "ARL"
--		L["AutohideDesc"] = "Авто-скрытие"
		L["AutohideName"] = "Авто-скрытие"
		L["AvailableProfiles"] = "Доступные профиля"
--		L["BackgroundDesc"] = "Фон"
		L["BackgroundName"] = "Фон"
		L["Bank"] = "Банк"
--		L["BorderDesc"] = "Границы"
		L["BorderName"] = "Границы"
		L["BottomLeft"] = "Внизу слева"
		L["BottomRight"] = "Внизу справа"
		L["CanMake"] = "Можно создать"
--		L["CanMakeDesc"] = "Описание: Можно создать" -- Needs review
		L["CanMakeName"] = "Можно создать"
		L["CannotCreate"] = "не возможно создать что-либо"
		L["Cast"] = "Применение"
		L["CloseMenu"] = "Закрыть"
		L["Cooldown"] = "Востоновление"
--		L["CraftTipsDesc"] = "Описание: Подсказки умений" -- Needs review
		L["CraftTipsName"] = "Подсказки умений"
		L["CreateAll"] = "Создать всё"
		L["CreateNum"] = "Создать еще"
		L["CreateOne"] = "Создать 1"
		L["DebugDesc"] = "Отображать отладочные сообщения. Только для тестирования."
		L["DebugName"] = "Отладка"
		L["Default"] = "По умолчанию"
		L["DeleteChar"] = "Удалить персонажа"
		L["Description"] = "Описание" -- Needs review
		L["EnabledDesc"] = "Активирует модификацию и скрывает стандартное окно Blizzard."
		L["EnabledName"] = "Включен"
		L["Favorite"] = "Избранное"
--		L["FavoriteDesc"] = "Описание: Избранное" -- Needs review
		L["FavoriteName"] = "Избранное"
		L["Grouping"] = "Группирование"
--		L["IconDesc"] = "Описание: Отображать иконку" -- Needs review
		L["IconName"] = "Отображать иконку"
--		L["LevelDesc"] = "Описание: Уровень" -- Needs review
		L["LevelName"] = "Уровень"
		L["LinkRecipe"] = "Ссылка рецепта!"
		L["LinkResult"] = "Ссылка результата"
		L["LockDesc"] = "Закрепить меню"
--		L["LockedDesc"] = "Описание: Фиксировать кнопки" -- Needs review
		L["LockedName"] = "Фиксировать кнопки"
		L["LockName"] = "Фиксировать кнопки"
		L["LowerSpells"] = "Показать низкоуровневые"
		L["MarketValue"] = "Цена:"
		L["MatsCost"] = "Цена матов"
--		L["MoneyDesc"] = "Описание: цена" -- Needs review
		L["MoneyName"] = "Суммарная цена"
--		L["OrangeDesc"] = "Описание: Оранжевые" -- Needs review
		L["OrangeName"] = "Оранжевые"
		L["Oranges"] = "Оранжевые рецепты"
		L["PickupAll"] = "Выбирать всё"
		L["PickupNum"] = "Выбирать еще"
		L["PickupOne"] = "выбирать 1"
		L["Presorting"] = "Предсортировка"
		L["Profile"] = "Профиль"
		L["ProfileCopied"] = "Настройки скопированны из: \"%s\""
		L["ProfileCreated"] = "Создан новый профиль: \"%s\""
		L["ProfileDeleted"] = "Удален профиль: \"%s\""
		L["ProfileLoaded"] = "Установлен профиль: \"%s\""
		L["ProfileReset"] = "Сброс профиля \"%s\""
--		L["ReagentTipsDesc"] = "Описание: Подсказки реагентов" -- Needs review
		L["ReagentTipsName"] = "Подсказки реагентов"
		L["SaveChar"] = "Сохранить персонажа"
		L["Search"] = "Поиск"
--		L["SearchDesc"] = "Поиск"
		L["SearchName"] = "Поиск"
--		L["SelectDesc"] = "Описание: Выбор" -- Needs review
		L["SelectName"] = "Выбор"
		L["ShowALR"] = "Показать Ackis Recipe List"
--		L["SmoothDesc"] = "Описание: Окраска рецептов по сложности" -- Needs review
		L["SmoothName"] = "Окраска рецептов по сложности"
		L["SortAlpha"] = "Альфовит"
--		L["SortDesc"] = "Описание: сортировка" -- Needs review
		L["SortDifficulty"] = "Сложность"
		L["SortLevel"] = "Уровень"
		L["SortName"] = "Сортировка"
		L["SortNone"] = "неотсортерованно"
		L["SwitchChar"] = "Смена персонажа"
--		L["SwitchCharDesc"] = "Описание: Смена персонажа" -- Needs review
		L["SwitchSelect"] = "Смена сортировки"
--		L["TabsDesc"] = "Описание: Отображать закладки" -- Needs review
		L["TabsName"] = "Отображать закладки"
		L["TimeoutDesc"] = "Истечение времени ожидания события"
		L["TimeoutName"] = "Простой"
		L["TopLeft"] = "Вверху слева"
		L["TopRight"] = "Вверху справа"
		L["Trades"] = "Профессии"
		L["Unknown"] = "Неизвестный"
		L["Unsorted"] = "Неотсортерованно"
		L["Vendor"] = "Тарговец"
		L["VendorValue"] = "У тарговца:"
		L["Version"] = "Версия"
		L["VersionDesc"] = "Вывод информации о текущей версии"
--		L["YellowDesc"] = "Описание: Желтые" -- Needs review
		L["YellowName"] = "Желтые"
		L["Yellows"] = "Желтые рецепты"
		L.Fast = "быстро"
		L.FastDesc = "первый слот сумки, |cff11ffffбез подтверждения|r"
	end
end

do
	local L = AL:NewLocale('Producer', 'koKR') -- I need help on this translation
	if L then 
	end
end

do
	local L = AL:NewLocale('Producer', 'zhCN') -- By CWOW二区四组PVE迷雾之海女圣徒
	if L then 
		L.AnchorDesc        = '为你的主窗口指定一个锚点位置。'
		L.AnchorName        = '锚点'
		L.ARL               = 'ARL'
		L.AutohideDesc      = '在指定的持续时间过后自动隐藏窗口。'
		L.AutohideName      = '自动隐藏'
		L.AvailableProfiles = '可用的配置文件'
		L.BackgroundDesc    = '选择你喜欢的背景。'
		L.BackgroundName    = '背景'
		L.Bank              = '银行'
		L.BorderDesc        = '选择你喜欢的边框'
		L.BorderName        = '边框'
		L.BottomLeft        = '左下'
		L.BottomRight       = '右下'
		L.CanMake           = '材料齐备'
		L.CanMakeDesc       = '显示所有材料齐备的配方。'
		L.CanMakeName       = '材料齐备'
		L.CannotCreate      = '无法制造'
		L.Cast              = '消耗'
		L.CloseMenu         = '关闭'
		L.Cooldown          = '技能冷却'
		L.CraftTipsDesc     = '为配方显示鼠标提示。'
		L.CraftTipsName     = '配方提示'
		L.CreateAll         = '全部制造'
		L.CreateNum         = '按数目制造'
		L.CreateOne         = '制造一个'
		L.DebugDesc         = '显示除错信息。 仅供测试使用。'
		L.DebugName         = '除错'
		L.Default           = '默认'
		L.DeleteChar        = '删除角色信息'
		L.Description       = 'Producer 是一个 Ace3 插件，替代 Blizzard 的商业技能窗口，并且提供了额外的功能。'
		L.EnabledDesc       = '决定该模块是否启用。'
		L.EnabledName       = '开启'
		L.Favorite          = '标记项目'
		L.FavoriteDesc      = '显示你标记的配方。'
		L.FavoriteName      = '标记项目'
		L.Grouping          = '分组'
		L.IconDesc          = '显示配方的图标。'
		L.IconName          = '显示图标'
		L.LevelDesc         = '配方等级与最高技能的差值。\n(recipe.skill >= max.skill - value)\n\n圣徒举例：\n技能等级上限375，数值填100：\n则显示技能等级在275以上的配方。\n默认值为0，即显示全部配方。'
		L.LevelName         = '技能等级差值'
		L.LinkRecipe        = '链接配方!'
		L.LinkResult        = '链接成品'
		L.LockedDesc        = '锁定窗口位置，无法拖动。'
		L.LockedName        = '位置锁定'
		L.LockDesc          = '点击可以锁定子菜单。'
		L.LockName          = '菜单锁定'
		L.LowerSpells       = '显示低等级技能'
		L.MarketValue       = '市场价格'
		L.MatsCost          = '材料成本'
		L.MoneyDesc         = '如果可能，显示材料成本预估。'
		L.MoneyName         = '成本预估'
		L.OrangeDesc        = '显示目前为橙色状态的配方。'
		L.OrangeName        = '橙色'
		L.Oranges           = '橙色配方'
		L.PickupAll         = '拾取全部'
		L.PickupNum         = '按数目拾取'
		L.PickupOne         = '拾取一个'
		L.Presorting        = '预排序'
		L.Profile           = '配置文件'
		L.ProfileCopied     = '从该配置文件复制设置 "%s"'
		L.ProfileCreated    = '生成新的配置文件 "%s"'
		L.ProfileDeleted    = '删除配置文件 "%s"'
		L.ProfileLoaded     = '选择配置文件 "%s"'
		L.ProfileReset      = '重置配置 "%s"'
		L.ReagentTipsDesc   = '在鼠标提示上显示材料'
		L.ReagentTipsName   = '材料提示'
		L.SaveChar          = '保存角色信息'
		L.Search            = '搜索'
		L.SearchDesc        = '显示搜索标签。'
		L.SearchName        = '显示搜索'
		L.SelectDesc        = '为你的专业选择一个预定的排序方式。'
		L.SelectName        = '选择排序'
		L.ShowALR           = '显示 Ackis Recipe List 插件支持'
		L.SmoothDesc        = '用不同颜色标示配方的技能等级。'
		L.SmoothName        = '技能等级标色'
		L.SortAlpha         = '按字母顺序排列（英文名称）'
		L.SortDesc          = '选择配方的排序方式，'
		L.SortDifficulty    = '难易程度'
		L.SortLevel         = '技能等级'
		L.SortName          = '排序'
		L.SortNone          = '无序'
		L.SwitchChar        = '切换角色'
		L.SwitchCharDesc    = '显示其他角色已保存的专业。'
		L.SwitchSelect      = '切换分类方式'
		L.TabsDesc          = '显示所有专业标签，以便快速切换。'
		L.TabsName          = '显示标签'
		L.TimeoutDesc       = '在指定的持续时间过后自动隐藏窗口。'
		L.TimeoutName       = '持续时间'
		L.Trades            = '专业'
		L.TopLeft           = '左上'
		L.TopRight          = '右上'
		L.Unknown           = '未知'
		L.Unsorted          = '无序'
		L.Vendor            = '售卖者'
		L.VendorValue       = '售卖价格:'
		L.Version           = '版本'
		L.VersionDesc       = '打印当前版本'
		L.YellowDesc        = '显示目前为黄色状态的配方。'
		L.YellowName        = '黄色'
		L.Yellows           = '黄色配方'
	end
end

do
	local L = AL:NewLocale('Producer', 'zhTW') -- I need help on this translation
	if L then 
	end
end
