--[[
Producer - a framework for World of Warcraft professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local AceGUI    = LibStub( 'AceGUI-3.0')
local AceTimer  = LibStub( 'AceTimer-3.0')
local Producer  = LibStub( 'AceAddon-3.0'):GetAddon( 'Producer')
local L         = LibStub( 'AceLocale-3.0'):GetLocale( 'Producer')
local Snowflake = LibStub( 'Snowflake-2.0')
local SM        = LibStub( 'LibSharedMedia-3.0')

local CraftFrame = AceGUI:Create( 'SnowflakeEscape')
Producer.CraftFrame = CraftFrame

local TradeSkillChangers = {}
local ICONS_PROFESSION  = {
	['Alchemy']		   = 'Interface\\Icons\\Trade_Alchemy',
	['Blacksmithing']  = 'Interface\\Icons\\Trade_BlackSmithing',
	['Cooking']		   = 'Interface\\Icons\\INV_Misc_Food_15',
	['Enchanting']	   = 'Interface\\Icons\\Trade_Engraving',
	['Engineering']	   = 'Interface\\Icons\\Trade_Engineering', 
	['First Aid']	   = 'Interface\\Icons\\Spell_Holy_SealOfSacrifice',
	['Inscription']    = 'Interface\\Icons\\INV_Inscription_Tradeskill01', 
	['Jewelcrafting']  = 'Interface\\Icons\\INV_Misc_Gem_02', 
	['Leatherworking'] = 'Interface\\Icons\\INV_Misc_ArmorKit_17',
--	['Mining']		   = 'Interface\\Icons\\Spell_Fire_FlameBlades',
	['Runeforge']	   = 'Interface\\Icons\\Spell_DeathKnight_FrozenRuneWeapon',
	['Smelting']       = 'Interface\\Icons\\Spell_Fire_FlameBlades',
	['Tailoring']	   = 'Interface\\Icons\\Trade_Tailoring',
}
local LINE_BORDER  = {
	bgFile = 'Interface\\Tooltips\\UI-Tooltip-Background',
	edgeFile = 'Interface\\Tooltips\\UI-Tooltip-Border', edgeSize = 16,
	insets = { left = 3, right = 3, top = 3, bottom = 3 },
}

local function sortIndex( a, b)
	if not a then return false end
	if not b then return true end
	if a.index == b.index then 
		return a.name < b.name 
	end
	return a.index < b.index 
end

local function sortLevel( a, b)
	if not a then return false end
	if not b then return true end
	if a.skill == b.skill then 
		return a.name < b.name 
	end
	return a.skill > b.skill
end

local function sortDifficulty( a, b)
	if not a then return false end
	if not b then return true end
	if a.difficulty == b.difficulty then 
		if a.skill == b.skill then 
			return a.name < b.name 
		end
		return a.skill > b.skill
	end
	return a.difficulty < b.difficulty
end

local function sortAlpha( a, b)
	if not a then return false end
	if not b then return true end
	return a.name < b.name 
end

local SORT = {
	['none']       = sortIndex,
	['level']      = sortLevel,
	['difficulty'] = sortDifficulty,
	['alpha']      = sortAlpha,
}
local COLORS = {
	'FF8844', -- orange
	'FFFF00', -- yellow
	'44CC44', -- green
	'AAAAAA', -- gray
}

-- 9D9D9D poor
-- FFFFFF common
-- 1EFF00 uncommon
-- 0070DD rare
-- A335EE epic
-- FF8000 legendary
-- FF0000 artifact
-- E5CC80 account

local MAX_INT = 999999
local showLower = false
local searchText = ''
local matchText
local thread
local repeater
local splitStack
local numberChanged

--[[---------------------------------------------------------------------------------
  Local Functions
------------------------------------------------------------------------------------]]
local function PrintLinkToChat( link)
	if type( link) == 'string' then
		for i = 1,NUM_CHAT_WINDOWS do
			local frame = _G['ChatFrame'..i]
			local editbox = frame and frame.editBox
			if editbox and editbox:IsVisible() then
				editbox:Insert( link)
				return
			end
		end 
		ChatFrame_OpenChat( link)
	end
end

local function NameOf( link)
	return link and link:match('|h%[?(.-)%]?|h') or 'unknown'
end

local function PutItem()
	local free = GetContainerNumFreeSlots( 0)
	if free > 0 then
		PutItemInBackpack()
		return
	end
	for bagID = 1,NUM_BAG_SLOTS do
		free = GetContainerNumFreeSlots( bagID)
		if free > 0 then
			PutItemInBag( 19 + bagID)
			return
		end
	end
	PutItemInBackpack()
end

local function PickUpTradeRun( reagents, num)
	local pickUp = {}
	for _,r in pairs(reagents) do
		local name = NameOf( r.link)
		pickUp[name] = r.need * num
		Producer:Debug( 'PickUpTradeRun', name, r.need, pickUp[name], r.link)
	end
	for i = 1,NUM_BANKBAGSLOTS do
		local bagID = NUM_BAG_SLOTS + i
		for slotID = 1, GetContainerNumSlots( bagID) do
			while true do
				local _, count, locked = GetContainerItemInfo( bagID, slotID)
				if locked then
					coroutine.yield()
				else
					local name = NameOf( GetContainerItemLink( bagID, slotID))
					local need = name and pickUp[name] or 0
					if need > 0 then 
						ClearCursor()
						if need >= count then
							Producer:Debug( 'PickUpTradeRun', 'PickupContainerItem', name, need, count)
							PickupContainerItem( bagID, slotID)
							pickUp[name] = need - count
						else
							Producer:Debug( 'PickUpTradeRun', 'SplitContainerItem', name, need, count)
							SplitContainerItem( bagID, slotID, need)
							pickUp[name] = 0
						end
						PutItem()
						ClearCursor()
					end
					break
				end
			end
		end
	end
	Producer:TRADE_SKILL_UPDATE()
end
	
local function PickUpResume()
	if type(thread) == 'thread' and coroutine.status( thread) == 'suspended' then
		coroutine.resume( thread)
	end
end

local function PickUpTrade( trade, num)
	if type(thread) ~= 'thread' or coroutine.status( thread) == 'dead' then
		Producer:Debug( 'PickUpTrade', trade.index, num)
		thread = coroutine.create( PickUpTradeRun)
		coroutine.resume( thread, trade.reagents or {}, num or 1)
	else
		Producer:Print( 'PickUpTrade', 'failed thread', type(thread))
	end
end

local function DoTrade( data, trade, count)
	data.repIndex = trade.index
	data.repCount = tonumber( count)
	if trade.cast then
		DoTradeSkill( trade.index, data.repCount)
	elseif splitStack then
		PickUpTrade( trade, data.repCount)
	else
		DoTradeSkill( trade.index, data.repCount)
	end
end

local function DoEnchBP( data, trade, count)
    if trade.cast then
		if GetContainerItemID(0,1) then
			DoTradeSkill(trade.index)
			ContainerFrame1Item16:Click()
			ReplaceEnchant()
			StaticPopup1Button1:Click()
		else
			Producer:Print( 'missing item in bag 1 slot 1')
		end
    end
end

local function SetSearch( obj)
	searchText = (type(obj) == 'string') and obj or ''
	matchText = (searchText ~= '') and searchText:lower() or nil
	if CraftFrame.menu then
		CraftFrame.menu:Refresh()
	end
end

local function ToggleFavorite( data, name)
	local f = Producer.db.global.favorites[data.prof] or {}
	if f[name] then
		f[name] = nil
	else
		f[name] = true
	end
	Producer.db.global.favorites[data.prof] = f
end

local function GetMaxMade( reagents)
	local made = MAX_INT
	local bank = MAX_INT
	for _,r in pairs(reagents) do
		if r.link and r.need > 0 then
			local rMade = math.floor(GetItemCount( r.link, false) / r.need)
			local rBank = math.floor(GetItemCount( r.link, true)  / r.need)
			made = (rMade < made) and rMade or made
			bank = (rBank < bank) and rBank or bank
		else
			made = 0
			bank = 0
		end
	end
	made = (made < MAX_INT) and made or 0
	bank = (bank < MAX_INT) and bank or 0
	return made, bank
end

local function GetMaxMadeStr( reagents)
	local made, bank = GetMaxMade( reagents)
	if bank > 0 then
		return format( '[%d/%d]', made, bank - made)
	end
end

-- sell, buy, buyout
local function GetPriceData( link)
	if not link then
		return 0, 0, 0
	end
	if not Producer.db.profile.money then 
		return nil, nil, nil 
	end
	if KC_Items and not KC_Items.common then
		local buyout = select(10, AuctionSpy:GetItemInfo( UnitFactionGroup( 'player'), AuctionSpy:GetLongCode( link)))
		local sell, buy = SellValues:GetPrices( link)
		return sell, buy, buyout
	end
	if KC_Items then
		local resultinfo = KC_Items.common:GetItemInfo( KC_Items.common:GetCode( link))
		local _, _, _, _, _, _, sBuy = KC_Auction:GetItemData( KC_Items.common:GetCode( link, true))
		return resultinfo['sell'], resultinfo['buy'], sBuy
	end
--	if Auctioneer and Auctioneer.HistoryDB and Auctioneer.Util and Auctioneer.ItemDB and Informant then
	if AucAdvanced and Informant then
--		local itemTotals = Auctioneer.HistoryDB.GetItemTotals( Auctioneer.ItemDB.CreateItemKeyFromLink( link), Auctioneer.Util.GetAuctionKey())
--		local avgBuy = 0
--		if itemTotals and itemTotals.buyoutCount > 0 then
--			avgBuy = math.floor( itemTotals.buyoutPrice / itemTotals.buyoutCount)
--		end
		local res1, avgBuy = pcall( AucAdvanced.API.GetMarketValue, link)
		local itemid = tonumber( link:match( 'item:(%d+)'))
		local res2, venddata = pcall( Informant.GetItem, itemid)
		if res1 and res2 and venddata then
			return venddata.sell, venddata.buy, avgBuy
		end
		if res1 then
			return 0, 0, avgBuy
		end
		return nil, nil, nil 
	end
end

local function round( val) 
	return math.floor( val + 0.5) 
end

local function GetColor( smooth, trade, rank)
	if smooth and trade.skill then
		local percent = trade.skill / rank
		if percent > 1 then
			percent = 1
		end
		if percent >= 0.6667 then
			--RANGE: FF8844 (TOP) to FFFF00 (BOTTOM)
			--  1.000,0.531,0.265 to 1.000,1.000,0.000
			return format( '%02X%02X%02X', 255, round(493 - percent * 357), round(percent * 204 - 136))
		elseif percent >= 0.3333 then
			--RANGE: FFFF00 (TOP) to 44CC44 (BOTTOM)
			--  1.000,1.000,0.000 to 0.265,0.786,0.265
			return format( '%02X%02X%02X', round(percent * 561 - 119), round(153 + percent * 153), round(136 - percent * 204))
		else
			--RANGE: 44CC44 (TOP) to AAAAAA (BOTTOM)
			--  0.265,0.786,0.265 to 0.664,0.664,0.664
			return format( '%02X%02X%02X', round(170 - percent * 306), round(170 + percent * 102), round(170 - percent * 306))
		end
	else
		return COLORS[trade.difficulty]
	end
end

local function GetMoneyText( cash)
	local g = floor( cash/10000)
	local s = mod( floor( cash/100), 100)
	local c = mod( cash, 100)
	if g > 0 then
		return format( '|cFFFFD700%dg|r|cFFC7C7CF%02ds|r|cFFEDA55F%02dc|r', g, s, c)
	elseif s > 0 then
		return format( '|cFFC7C7CF%ds|r|cFFEDA55F%02dc|r', s, c)
	elseif c > 0 then
		return format( '|cFFEDA55F%dc|r', c)
	else
		return ''
	end
end

--[[---------------------------------------------------------------------------------
  Menu Functions
------------------------------------------------------------------------------------]]
local function AddItemLine( trade, grp)
	local v = { key = trade.name, action = HandleModifiedItemClick, params = { trade.link } }
	if trade.max and trade.max > trade.min then
		v.text = format( '|cFF%s%d-%d %s|r', COLORS[trade.difficulty], trade.min, trade.max, trade.name)
	elseif trade.min and trade.min > 1 then
		v.text = format( '|cFF%s%d %s|r', COLORS[trade.difficulty], trade.min, trade.name)
	else
		v.text = format( '|cFF%s%s|r', COLORS[trade.difficulty], trade.name)
	end
	local count = GetMaxMadeStr( trade.reagents)
	if trade.skill then
		v.second = format( '%s (%s)', count or '', trade.skill)
	else
		v.second = count
	end
	if Producer.db.profile.icons and trade.link then
		v.icon = select( 10, GetItemInfo( trade.link))
	end
	if Producer.db.profile.craftTips then
		v.link = trade.link
	end
	grp:AddLine( v)
end

local CreateCrafted

local function AddItemReagentLine( data, reag, price, grp)
	local v = { key = reag.name, action = HandleModifiedItemClick, params = { reag.link } }
	local case = reag.link and GetItemCount( reag.link, false) or 0
	local bank = reag.link and GetItemCount( reag.link, true) or 0
	if reag.vendor then
		v.text = format('|cFF6666FF%s|r [%d/%d] (%s)', reag.name, case, reag.need, L.Vendor)
	elseif bank > case then
		v.text = format('|cFF6666FF%s|r [%d/%d] (%s: %d)', reag.name, case, reag.need, L.Bank, bank - case)
	else
		v.text = format('|cFF6666FF%s|r [%d/%d]', reag.name, case, reag.need)
	end
	v.second = price
	local trade = data.profession[reag.name]
	if trade then
		v.hasArrow = true
		v.func = CreateCrafted
		v.args = { data, trade }
	end
	if Producer.db.profile.icons and reag.link then
		v.icon = select( 10, GetItemInfo( reag.link))
	end
	if Producer.db.profile.reagentTips then
		v.link = reag.link
	end
	grp:AddLine( v)
end

function CreateCrafted( data, trade, grp)
	AddItemLine( trade, grp)
	if Producer:GetProfession( data.prof, 'favorite') then
		local f = Producer.db.global.favorites[data.prof] or {}
		grp:AddLine{ text = L.Favorite, action = ToggleFavorite, params = { data, trade.name }, checked = f[trade.name]  }
	end
	if not trade.cast and Producer.db.profile.money and trade.link then
		local sell, buy, buyout = GetPriceData( trade.link)
		if buyout and buyout > 0 then
			grp:AddLine{ text = L.MarketValue, second = GetMoneyText( buyout), type = 'description' }
		end
		if sell and sell > 0 then
			grp:AddLine{ text = L.VendorValue, second = GetMoneyText( sell), type = 'description' }
		end
	end
	grp:AddLine()
	if not data.isLink then
		local made, bank = GetMaxMade( trade.reagents)
		if splitStack and bank > made then
			local val = numberChanged or 1
			grp:AddLine{ text = format( '|cFF00FF00%s|r', L.PickupOne), action = DoTrade, params = { data, trade } }
			grp:AddLine{ text = format( '|cFF00FF00%s|r', L.PickupNum), action = DoTrade, params = { data, trade }, editNumber = val, type = 'input' }
			grp:AddLine{ text = format( '|cFF00FF00%s|r', L.PickupAll), action = DoTrade, params = { data, trade, bank - made } }
		elseif trade.cast then
			grp:AddLine{ text = format( '|cFF00FF00%s|r', trade.cast), action = DoTrade, params = { data, trade } }
            grp:AddLine{ text = format( '|cFF00FF00%s|r', trade.cast .. " " .. L.Fast), action = DoEnchBP, params = { data, trade },second = L.FastDesc}
		else
			local val = (trade.index == data.repIndex) and data.repCount or 1
			grp:AddLine{ text = format( '|cFF00FF00%s|r', L.CreateOne), action = DoTrade, params = { data, trade } }
			grp:AddLine{ text = format( '|cFF00FF00%s|r', L.CreateNum), action = DoTrade, params = { data, trade }, editNumber = val, type = 'input' }
			grp:AddLine{ text = format( '|cFF00FF00%s|r', L.CreateAll), action = DoTrade, params = { data, trade, made } }
		end
		grp:AddLine()
		local tools = BuildColoredListString( GetTradeSkillTools( trade.index))
		if tools then
			grp:AddLine{ key = 'Tools', text = tools, type = 'description', maxWidth = 40 }
		end
		local cooldown = GetTradeSkillCooldown( trade.index)
		if cooldown and cooldown > 0 then
			grp:AddLine{ key = 'Cooldown', text = format( '|cFFFF3333%s: %s|r', L.Cooldown, SecondsToTime( cooldown)), type = 'description' }
		end
	end
	local total = 0
	local isTotal = true
	for _,r in pairs(trade.reagents) do
		local price
		if Producer.db.profile.money and r.link then
			local sell, buy, buyout = GetPriceData( r.link)
			if buy and buy > 0 and not (buyout and buyout ~= 0 and Auctioneer) then
				price = buy * r.need
				total = total + price
				price = GetMoneyText( price)
			elseif buyout and buyout > 0 then
				price = buyout * r.need
				total = total + price
				price = GetMoneyText( price)
			else
				price = ''
				isTotal = false
			end
		else
			price = ''
			isTotal = false
		end
		AddItemReagentLine( data, r, price, grp)
	end
	if isTotal and total > 0 then
		grp:AddLine{ text = L.MatsCost, second = GetMoneyText( total), type = 'description' }
	end
	grp:AddLine()
	CraftFrame:AddTradeSkillLines( trade, grp)
	if trade.link then
		grp:AddLine{ text = format( '|cFFFFFF00%s|r', L.LinkResult), action = PrintLinkToChat, params = { trade.link } }
	end
	if trade.recipe then
		grp:AddLine{ text = format( '|cFFFFFF00%s|r', L.LinkRecipe), action = PrintLinkToChat, params = { trade.recipe } }
	end
end

local function AddTradeLines( data, trades, grp)
	if table.getn(trades) == 0 then
		grp:AddLine{ text = L.CannotCreate, type = 'description' }
	else
		table.sort( trades, SORT[Producer:GetProfession( data.prof, 'sort')])
		for _,t in ipairs(trades) do
			local v = { key = t.name, func = CreateCrafted, args = { data, t }, hasArrow = true, action = HandleModifiedItemClick, params = { t.link }  }
			v.text = format( '|cFF%s%s|r', GetColor( Producer:GetProfession( data.prof, 'smooth'), t, data.rank), t.name)
			v.second = GetMaxMadeStr( t.reagents)
			if Producer.db.profile.icons and t.link then
				v.icon = select( 10, GetItemInfo( t.link))
			end
			if Producer.db.profile.craftTips then
				v.link = t.link
			end
			v.alpha = matchText and not t.name:lower():match( matchText) and 0.2
			for _,fkt in ipairs( TradeSkillChangers) do                                                                           
				fkt( t, v)                                                                                                 
			end                                                                                                                
			grp:AddLine( v)
		end
	end
end

local function GetMinSkill( data)
	local l = tonumber(Producer:GetProfession( data.prof, 'level')) or 0
	return l > 0 and l < data.rank and (data.rank - l) or 0
end

local function GetGroups( data)
	local result = {}
	local added = {}
	local matches = {}
	for k,v in pairs(data.profession) do
		if v.set and not added[v.set] then
			table.insert( result, v.set)
			added[v.set] = true
		end
		if matchText and v.name:lower():match( matchText) then
			matches[v.set] = true
		end
	end
	table.sort( result)
	return result, matches
end

local function CreateSet( data, set, grp)
	local minSkill = GetMinSkill( data)
	local trades = {}
	for _,v in pairs(data.profession) do
		if v.set == set and (showLower or v.skill >= minSkill) then
			table.insert( trades, v)
		end
	end
	AddTradeLines( data, trades, grp)
end

local function CreateOranges( data, grp)
	local minSkill = GetMinSkill( data)
	local trades = {}
	for _,v in pairs(data.profession) do
		if v.difficulty == 1 and (showLower or v.skill >= minSkill) then
			table.insert( trades, v)
		end
	end
	AddTradeLines( data, trades, grp)
end

local function CreateYellows( data, grp)
	local minSkill = GetMinSkill( data)
	local trades = {}
	for _,v in pairs(data.profession) do
		if v.difficulty == 2 and (showLower or v.skill >= minSkill) then
			table.insert( trades, v)
		end
	end
	AddTradeLines( data, trades, grp)
end

local function CreateCanMake( data, grp)
	local minSkill = GetMinSkill( data)
	local trades = {}
	for _,v in pairs(data.profession) do
		if showLower or v.skill >= minSkill then
			local _, bank = GetMaxMade( v.reagents)
			if bank > 0 then
				table.insert( trades, v)
			end
		end
	end
	AddTradeLines( data, trades, grp)
end

local function CreateFavorite( data, grp)
	local f = Producer.db.global.favorites[data.prof] or {}
	local trades = {}
	for _,v in pairs(data.profession) do
		if f[v.name] then
			table.insert( trades, v)
		end
	end
	AddTradeLines( data, trades, grp)
end

local function CreateMenu( data, grp)
--	Producer:Debug( 'CreateMenu', title)
	grp:AddLine{ text = data.title, type = 'header' }
--	local isSearch = Producer:GetProfession( data.prof, 'search')
--	if isSearch then  -- 'Interface\\Minimap\\Tracking\\None', 'Interface\\LFGFrame\\LFGFrame-SearchIcon-Background'
--		grp:AddLine{ text = L.Search, action = SetSearch, params = {}, editText = searchText, editWidth = 100, type = 'input', icon = 'Interface\\SpellShadow\\Spell-Shadow-Unacceptable' }
--	end
	grp:AddLine()
	local trades, matches = GetGroups( data)
	for _,set in pairs(trades) do
		local alpha = matchText and not matches[set] and 0.3
		grp:AddLine{ text = set, func = CreateSet, args = { data, set }, alpha = alpha, hasArrow = true }
	end
	local isOrange = Producer:GetProfession( data.prof, 'orange')
	local isYellow = Producer:GetProfession( data.prof, 'yellow')
	if not data.isLink and (isOrange or isYellow) then
		grp:AddLine()
		if isOrange then
			grp:AddLine{ text = format( '|cFFFF8844%s|r', L.Oranges), func = CreateOranges, args = { data }, hasArrow = true }
		end
		if isYellow then
			grp:AddLine { text = format( '|cFFFFE000%s|r', L.Yellows), func = CreateYellows, args = { data }, hasArrow = true }
		end
	end
	local isCanMake  = Producer:GetProfession( data.prof, 'canmake')
	local isFavorite = Producer:GetProfession( data.prof, 'favorite')
	if isCanMake or isFavorite then
		grp:AddLine()
		if isCanMake then
			grp:AddLine{ text = format( '|cFF44CC44%s|r', L.CanMake), func = CreateCanMake, args = { data }, hasArrow = true }
		end
		if isFavorite then
			grp:AddLine{ text = format( '|cFFA335EE%s|r', L.Favorite), func = CreateFavorite, args = { data }, hasArrow = true }
		end
	end
end

--[[---------------------------------------------------------------------------------
  Main
------------------------------------------------------------------------------------]]
local function Control_OnEnter( ctrl, name, frame)
--	Producer:Debug( 'Control_OnEnter')
	if frame.tooltip then
		GameTooltip:SetOwner( frame, 'ANCHOR_TOPLEFT');
		GameTooltip:SetText( frame.tooltip, nil, nil, nil, nil, 1);
		GameTooltip:Show();
	end
end

local function Control_OnLeave()
	GameTooltip:Hide();
end

local mouseDown
local function Control_OnMouseDown()
	if CraftFrame.frame:IsMovable() then
		CraftFrame.frame:StartMoving()
	end
	mouseDown = true
end

local function Control_OnMouseUp()
	CraftFrame.frame:StopMovingOrSizing()
	if CraftFrame.frame:IsMovable() then
		local pos = Producer.db.profile.anchor
		if pos == 'TopRight' then
			Producer.db.profile.xPos = CraftFrame.frame:GetRight()
			Producer.db.profile.yPos = CraftFrame.frame:GetTop()
		elseif pos == 'TopLeft' then
			Producer.db.profile.xPos = CraftFrame.frame:GetLeft()
			Producer.db.profile.yPos = CraftFrame.frame:GetTop()
		elseif pos == 'BottomRight' then
			Producer.db.profile.xPos = CraftFrame.frame:GetRight()
			Producer.db.profile.yPos = CraftFrame.frame:GetBottom()
		else
			Producer.db.profile.xPos = CraftFrame.frame:GetLeft()
			Producer.db.profile.yPos = CraftFrame.frame:GetBottom()
		end
	end
	mouseDown = true
end

local function Control_OnTextChanged( ctrl, name, btn, txt)
	Producer:Debug( 'Control_OnTextChanged', btn:GetUserData( 'key'), txt)
	if txt and txt ~= searchText then
		SetSearch( txt)
	end
end

local function Control_OnNumberChanged( ctrl, name, btn, txt)
	Producer:Debug( 'Control_OnNumberChanged', btn:GetUserData( 'key'), txt)
	if txt then
		numberChanged = tonumber( txt)
	end
end

local function Control_OnClose()
	CraftFrame:Hide()
end

local function Control_OnLink()
	PrintLinkToChat( GetTradeSkillListLink())
end

local function Control_OnTimeOut()
	if Producer.db.profile.timeout > 0 then
		if CraftFrame.menu then
			CraftFrame.menu:CloseNeighbour()
		end
		if Producer.db.profile.autohide then
			CraftFrame:Hide()
		end
	end
end

local function Control_OnSave( self)
	local saved = not self:GetUserData( 'saved')
	self:SetSaved( saved)
	Producer:SaveChar( saved)
end

local function Control_OnSwitch()
--	Producer:Debug( 'Control_OnSwitch')
	SetSearch( nil)
	if CraftFrame.menu then
		CraftFrame.menu:CloseNeighbour()
	end
	if CraftFrame.head then
		Producer:SwitchCharOpen( CraftFrame.head)
	end
end

local function Control_OnSelect()
--	Producer:Debug( 'Control_OnSelect')
	if CraftFrame.menu then
		CraftFrame.menu:CloseNeighbour()
	end
	if CraftFrame.head then
		Producer:SwitchSelectOpen( CraftFrame.head)
	end
end

local function Control_OnTabClick( ctrl, name, frame)
	SetSearch( nil)
	PlaySound( 'igCharacterInfoOpen')
	Producer:CastSpellOf( frame.prof)
end

local function Control_OnLower()
--	Producer:Debug( 'Control_OnLower')
	showLower = not showLower
	Producer:UpdateFrame()
end

local function CreateHeadFrame( self)
	if not self.head then
		local head = AceGUI:Create( 'ProducerHead')
		head.width = 'fill'
		head:SetCallback( 'OnClose',  Control_OnClose)
		head:SetCallback( 'OnLink',   Control_OnLink)
		head:SetCallback( 'OnSave',   Control_OnSave)
		head:SetCallback( 'OnSwitch', Control_OnSwitch)
		head:SetCallback( 'OnSelect', Control_OnSelect)
		head:SetCallback( 'OnEnter',  Control_OnEnter)
		head:SetCallback( 'OnLeave',  Control_OnLeave)
		head:SetCallback( 'OnMouseDown', Control_OnMouseDown)
		head:SetCallback( 'OnMouseUp',   Control_OnMouseUp)
		head:SetCallback( 'OnTabClick',  Control_OnTabClick)
		head:SetCallback( 'OnLower', Control_OnLower)
		self.head = head
		self:AddChild( head)
	end
end

local function CreateSearchFrame( self)
	if not self.search then
		local search = Snowflake:CreateMain()
		search.width = 'fill'
		search:SetUserData( 'hideSelect', true)
		search:SetCallback( 'OnMouseDown',   Control_OnMouseDown)
		search:SetCallback( 'OnMouseUp',     Control_OnMouseUp)
		search:SetCallback( 'OnTextChanged', Control_OnTextChanged)
		self.search = search
		self:AddChild( search)
	end
end

local function DeleteSearchFrame( self)
	if self.search then
		for k,v in pairs( self.children) do
			if v == self.search then
				tremove( self.children, k)
				break
			end
		end
		AceGUI:Release( self.search)
		self.search = nil
	end
end

local function CreateMenuFrame( self)
	if not self.menu then
		local menu = Snowflake:CreateMain()
		menu.width = 'fill'
		menu:SetCallback( 'OnClose',       Control_OnClose)
		menu:SetCallback( 'OnTimeOut',     Control_OnTimeOut)
		menu:SetCallback( 'OnMouseDown',   Control_OnMouseDown)
		menu:SetCallback( 'OnMouseUp',     Control_OnMouseUp)
		menu:SetCallback( 'OnTextChanged', Control_OnNumberChanged)
		self.menu = menu
		self:AddChild( menu)
	end
end

local function Control_OnShow()
	PlaySound( 'igCharacterInfoOpen')
end

local function Control_OnHide()
	Producer:Debug( 'Control_OnHide')
	if repeater then
		AceTimer:CancelTimer( repeater, true)
		repeater = nil
	end
	CraftFrame:ReleaseChildren()
	CraftFrame.head = nil
	CraftFrame.search = nil
	CraftFrame.menu = nil
	splitStack = false
	CloseTradeSkill()
	PlaySound( 'igCharacterInfoClose')
end

local function CreateSearch( grp)
	grp:AddLine{ text = L.Search, action = SetSearch, params = {}, editText = searchText, editWidth = 100, type = 'input', icon = 'Interface\\SpellShadow\\Spell-Shadow-Unacceptable' }
--	grp:AddLine{ text = L.Search, editText = searchText, editWidth = 100, type = 'input', icon = 'Interface\\SpellShadow\\Spell-Shadow-Unacceptable' }
end

local function PrintProfession( index)
--	local name, icon, skillLevel, maxSkillLevel, numAbilities, spelloffset, skillLine, skillModifier = GetProfessionInfo(index)
	if index then
		Producer:Debug( 'PrintProfession', GetProfessionInfo(index))
	else
		Producer:Debug( 'PrintProfession', 'unknown', index)
	end
end

local function AddTab( head, index, prof)
	if prof then
		local name, icon, skillLevel, maxSkillLevel, numAbilities, spelloffset, skillLine, skillModifier = GetProfessionInfo( prof)
		Producer:Debug( 'AddTab', name, icon, skillLevel, maxSkillLevel, numAbilities, spelloffset, skillLine, skillModifier)
		local spellIndex = spelloffset + 1
		local texture = GetSpellBookItemTexture( spellIndex, SpellBookFrame.bookType)
		local type, id = GetSpellBookItemInfo( spellIndex, SpellBookFrame.bookType)
		Producer:Debug( 'AddTab', type, id)
		head:ShowTab( index, name, name, texture)
		return index + 1
	end
	return index
end

local function ShowTabs( head)
	head:ClearTabs()
	if Producer.db.profile.tabs then
		local prof1, prof2, archaeology, fishing, cooking, firstAid = GetProfessions()
		Producer:Debug( 'ShowTabs1', prof1, prof2, archaeology, fishing, cooking, firstAid)
		local index = 1
		index = AddTab( head, index, firstAid)
		index = AddTab( head, index, cooking)
--		index = AddTab( head, index, fishing)
--		index = AddTab( head, index, archaeology)
		index = AddTab( head, index, prof2)
		index = AddTab( head, index, prof1)
--[[
		for i = 1,max do
			local type, id = GetSpellBookItemInfo( i, BOOKTYPE_SPELL)
			Producer:Debug( 'ShowTabs2', type, id)
			local prof = profs[id]
			if prof then
				local name = GetSpellInfo( id)
				head:ShowTab( index, prof, name, ICONS_PROFESSION[prof])
				index = index + 1
			end
		end
--]]
	end
end

local function RepeatEvent()
	local split = IsModifiedClick( 'SPLITSTACK')
	if splitStack ~= split then
		splitStack = split
		Producer:TRADE_SKILL_UPDATE()
	end
end

--[[---------------------------------------------------------------------------------
  CraftFrame
------------------------------------------------------------------------------------]]
function CraftFrame:Show()
--	Producer:Debug( 'CraftFrame:Show')
	if not self.head then
		self:SetLayout( 'ListZoom')
		self:SetCallback( 'OnUpdate', PickUpResume)
		self:SetCallback( 'OnShow',   Control_OnShow)
		self:SetCallback( 'OnHide',   Control_OnHide)
	end
	if not repeater then
		repeater = AceTimer:ScheduleRepeatingTimer( RepeatEvent, 0.1)
	end
	self.frame:Show()
end

function CraftFrame:Hide()
	self.frame:Hide()
end

function CraftFrame:Update( data, xPos, yPos, locked)
--	Producer:Debug( 'CraftFrame:Update')
	CreateHeadFrame( self)
	if Producer:GetProfession( data.prof, 'search') then
		CreateSearchFrame( self)
	else
		DeleteSearchFrame( self)
	end
	CreateMenuFrame( self)
	local edgeFile = SM:HashTable( 'border')[Producer.db.profile.border or 'Blizzard Tooltip']
	local bgFile   = SM:HashTable( 'background')[Producer.db.profile.background or 'Blizzard Tooltip']
	local head = self.head
	if head then
		ShowTabs( head)
		head:SetBorder( edgeFile, bgFile)
		head:SetLogo( data.prof and ICONS_PROFESSION[ data.prof] or ICONS_PROFESSION.Blacksmithing)
		head:SetSelect( data.select or '')
		head:SetLower( GetMinSkill( data) > 0, showLower)
		if data.isLink then
			head:SetLink( false)
			head:SetARL( false)
			head:SetSaved( data.saved)
			head:SetCharacter( data.player or '', 1, 0, 0)
		else
			head:SetLink( true)
			head:SetARL( true)
			head:SetSaved( nil)
			head:SetCharacter( data.player or '', 0, 1, 0)
		end
	end
	local search = self.search
	if search then
		Snowflake:SetBorder( search, edgeFile, bgFile)
		search:SetFunction( CreateSearch, {})
		search:Refresh()
	end
	local menu = self.menu
	if menu then
		if not Producer.db.profile.lock then
			menu:SetLock( false)
		end
		menu:SetUserData( 'lockEnabled', Producer.db.profile.lock)
		Snowflake:SetBorder( menu, edgeFile, bgFile)
		menu:SetFunction( CreateMenu, { data })
		menu:Refresh()
		menu:StartTimer( Producer.db.profile.timeout)
	end
	self:DoLayout()
	local frame = self.frame
	frame:StopMovingOrSizing()
	frame:ClearAllPoints()
	local pos = Producer.db.profile.anchor
	if pos == 'TopRight' then
		frame:SetPoint( 'TOPRIGHT', frame:GetParent(), 'BOTTOMLEFT', xPos, yPos)
	elseif pos == 'TopLeft' then
		frame:SetPoint( 'TOPLEFT', frame:GetParent(), 'BOTTOMLEFT', xPos, yPos)
	elseif pos == 'BottomRight' then
		frame:SetPoint( 'BOTTOMRIGHT', frame:GetParent(), 'BOTTOMLEFT', xPos, yPos)
	else
		frame:SetPoint( 'BOTTOMLEFT', frame:GetParent(), 'BOTTOMLEFT', xPos, yPos)
	end
	frame:SetMovable( not locked)
end


-----------------------------------------------------------------------------------
-- Hooking
-----------------------------------------------------------------------------------

-- Global registration for change function to the line of a single trade item
-- Functions registered with this function will be called with two arguments:
-- the data about the skill that is described as a table and the table as will
-- be given to SnowFlake's AddLine function.
--
-- The first table has the following layout:
-- - difficulty : one of 1 (optimal), 2 (medium), 3 (easy), 4 (trivial)
-- - index : the index of the trade skill for use with Blizzard functions
-- - name : name as returned by GetTradeSkillInfo
-- - cast : cast as returned by GetTradeSkillInfo (a verb or nil if 'Create' is to be used)
-- - link : as returned by GetTradeSkillItemLink
-- - recipe : as returned by GetTradeSkillRecipeLink
-- - min, max : as returned by GetTradeSkillNumMade, or 1
-- - reagents : a table with the reagents:
-- -- name : name of the reagent as returned by GetTradeSkillReagentInfo
-- -- link : link to the reagent as returned by GetTradeSkillReagentItemLink
-- -- need : the amount of the reagent required, as returned by GetTradeSkillReagentInfo
-- - set : the set the trade skill falls under; defaults to the trade (alchemy, enchanting...)
--         involved, but is set by the selector if one is used. A translation of Unsorted if
--         not in a set.
-- - skill : the skill level in the involved trade required to create the item.
--           Defaults to 450 if not known.
function CraftFrame:RegisterTradeSkillChanger( func)
	table.insert( TradeSkillChangers, func)
end

function CraftFrame:UnregisterTradeSkillChanger( func)
	for i,f in ipairs( TradeSkillChangers) do
		if f == func then
			table.remove( TradeSkillChangers, i)
			return
		end
	end
	Producer:Debug( 'Could not unregister function.')
end

-- An empty function for hooking.
-- This function will be called before the separator before the 'Link this/Link that'
-- is created for the item menus. The first argument is the usual self. The second
-- argument is a trade skill table as described with RegisterTradeSkillChangers above.
-- The third is the group that's being build. This is a SnowFlake menu and this
-- function should really only use the grp:AddLine() method of grp.
function CraftFrame:AddTradeSkillLines( trade, grp)
end
