--[[
Producer - a framework for World of Warcraft professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local Snowflake = LibStub( 'Snowflake-2.0')
local Producer = LibStub( 'AceAddon-3.0'):GetAddon( 'Producer')
local L = LibStub( 'AceLocale-3.0'):GetLocale( 'Producer')

local CharFrame = Snowflake:CreateMain()
CharFrame.frame:Hide()
Producer.CharFrame = CharFrame

local function HideFrame()
	CharFrame.frame:Hide()
	CharFrame:CloseNeighbour()
end

local function CreateUsers( names, prof, grp)
	local user = UnitName( 'player')
	local temp = {}
	for name,v in pairs(names) do
		if v then
			table.insert( temp, name)
		end
	end
	table.sort( temp)
	for _,n in pairs(temp) do
		local rank, maxRank
		local data = names[n]
		if type(data) == 'table' then
			rank = data.rank
			maxRank = data.maxRank
		else
			rank = data:match( 'rank["%]]*=(%d+)')
			maxRank = data:match( 'maxRank["%]]*=(%d+)')
		end
		local skill = rank and maxRank and format( '[%d,%d]', rank, maxRank) or nil
		if n == user then
			local player = format( '|cFF00FF00%s|r', n)
			grp:AddLine{ text = player, second = skill, action = 'CastSpellOf', params = { Producer, prof }, closeWhenClicked = true }
		else
			grp:AddLine{ text = n, second = skill, action = 'SwitchToChar', params = { Producer, prof, n }, closeWhenClicked = true }
		end
	end
end

local function CreateList( professions, grp)
	grp:AddLine{ text = L.SwitchChar, type = 'description' }
	grp:AddLine()
	local temp = {}
	for prof,v in pairs(professions) do
		if v and next(v) then
			table.insert( temp, prof)
		end
	end
	table.sort( temp)
	for _,n in pairs(temp) do
		local names = professions[n] or {}
		grp:AddLine{ text = Producer:GetProfessionName( n), func = CreateUsers, args = { names, n }, hasArrow = true }
	end
	grp:AddLine()
	grp:AddLine{ text = L.CloseMenu, action = HideFrame, params = {} }
end

function CharFrame:Show( grp, professions)
	self.parent = grp
	self.frame:SetParent( grp.frame)
	self:SetCallback( 'OnClose', HideFrame)
	self:SetCallback( 'OnTimeOut', HideFrame)
	self:SetFunction( CreateList, { professions })
	self:Refresh()
	local dx = grp.frame:GetLeft() + grp.frame:GetRight() - GetScreenWidth()
	local p = (dx >= 0) and 'TOPRIGHT' or 'TOPLEFT'
	local r = (dx >= 0) and 'TOPLEFT' or 'TOPRIGHT'
	self.frame:ClearAllPoints()
	self.frame:SetPoint( p, grp.frame, r)
	self.frame:Show()
	self:StartTimer( 3.0)
end

function CharFrame:Hide()
	HideFrame()
end

