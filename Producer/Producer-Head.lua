--[[
Producer - a framework for World of Warcraft professions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local Type, Version = 'ProducerHead', 2
local AceGUI = LibStub( 'AceGUI-3.0')
local ARL    = LibStub( 'AceAddon-3.0'):GetAddon( 'Ackis Recipe List', true)
local L      = LibStub( 'AceLocale-3.0'):GetLocale( 'Producer')

local PANE_BACKDROP = {
	bgFile = 'Interface\\Tooltips\\UI-Tooltip-Background',
	edgeFile = 'Interface\\Tooltips\\UI-Tooltip-Border', edgeSize = 16,
	insets = { left = 3, right = 3, top = 3, bottom = 3 },
}

--[[-----------------------------------------------------------------------------
Scripts
-------------------------------------------------------------------------------]]
local function Control_OnMouseDown( frame)
	frame.obj:Fire( 'OnMouseDown', frame)
end

local function Control_OnMouseUp( frame)
	frame.obj:Fire( 'OnMouseUp', frame)
end

local function Control_OnClose( frame)
	frame.obj:Fire( 'OnClose')
end

local function Control_OnLink( frame)
	frame.obj:Fire( 'OnLink')
end

local function Control_OnLower( frame)
	frame.obj:Fire( 'OnLower')
end

local function Control_OnSave( frame)
	frame.obj:Fire( 'OnSave')
end

local function Control_OnSwitch( frame)
	frame.obj:Fire( 'OnSwitch')
end

local function Control_OnSelect( frame)
	frame.obj:Fire( 'OnSelect')
end

local function Control_OnEnter( frame)
	frame.obj:Fire( 'OnEnter', frame)
end

local function Control_OnLeave( frame)
	frame.obj:Fire( 'OnLeave', frame)
end

local function Control_OnTabClick( frame)
	frame.obj:Fire( 'OnTabClick', frame)
end

--[[-----------------------------------------------------------------------------
Methods
-------------------------------------------------------------------------------]]
local methods = {
	['OnAcquire'] = function( self)
	end,

	['OnRelease'] = function( self)
	end,

	['SetLogo'] = function( self, url)
		self.logo:SetTexture( url or 'Interface\\Icons\\Trade_BlackSmithing')
	end,

	['SetCharacter'] = function( self, text, r, g, b)
		self.char.text:SetText( text or '')
		if r and g and b then
			self.char.text:SetTextColor( r, g, b)
		end
	end,

	['SetSelect'] = function( self, text)
		self.select.text:SetText( text or '')
	end,

	['SetLink'] = function( self, enabled)
		if enabled then
			self.link:Show()
		else
			self.link:Hide()
		end
	end,

	['SetARL'] = function( self, enabled)
		if ARL and ARL.scan_button then
			if enabled then
				ARL.scan_button:Show()
			else
				ARL.scan_button:Hide()
			end
		end
	end,

	['SetLower'] = function( self, enabled, pushed)
		if enabled then
			if pushed then
				self.lower:SetNormalTexture( 'Interface\\Minimap\\UI-Minimap-ZoomOutButton-Up')
			else
				self.lower:SetNormalTexture( 'Interface\\Minimap\\UI-Minimap-ZoomInButton-Up')
			end
			self.lower:Show()
		else
			self.lower:Hide()
		end
	end,

	['SetSaved'] = function( self, saved)
		self:SetUserData( 'saved', saved)
		if saved == nil then
			self.save:Hide()
		elseif saved then
			self.save:SetNormalTexture( 'Interface\\Icons\\INV_Gizmo_SuperSapperCharge')
			self.save.tooltip = L.DeleteChar
			self.save:Show()
		else
			self.save:SetNormalTexture( 'Interface\\Icons\\INV_Misc_Bag_28_Halloween')
			self.save.tooltip = L.SaveChar
			self.save:Show()
		end
	end,

	['ClearTabs'] = function( self)
		for i = 1,6 do
			self.tabs[i]:Hide()
		end
	end,

	['ShowTab'] = function( self, i, prof, tooltip, image)
		local tab = self.tabs[i]
		if tab then
			tab.prof = prof
			tab.tooltip = tooltip
			tab:SetNormalTexture( image)
			tab:Show()
		end
	end,

	['GetMinWidth'] = function( self)
		return self.minWidth + self.char.text:GetStringWidth()
	end,

	['SetBorder'] = function( self, border, background)
		if border then
			PANE_BACKDROP.edgeFile = border
		end
		if background then
			PANE_BACKDROP.bgFile = background
		end
		self.frame:SetBackdrop( PANE_BACKDROP)
		self.frame:SetBackdropColor( TOOLTIP_DEFAULT_BACKGROUND_COLOR.r, TOOLTIP_DEFAULT_BACKGROUND_COLOR.g, TOOLTIP_DEFAULT_BACKGROUND_COLOR.b)
	end
}

--[[-----------------------------------------------------------------------------
Constructor
-------------------------------------------------------------------------------]]
local function Constructor()
	local id  = AceGUI:GetNextWidgetNum( Type)
	
	local frame = CreateFrame( 'Frame', Type .. id, UIParent)
	frame:SetBackdrop( PANE_BACKDROP)
	frame:SetBackdropColor( TOOLTIP_DEFAULT_BACKGROUND_COLOR.r, TOOLTIP_DEFAULT_BACKGROUND_COLOR.g, TOOLTIP_DEFAULT_BACKGROUND_COLOR.b)
	frame:SetWidth( 100)
	frame:SetHeight( 48)
	frame:SetAlpha( 1)
	frame:EnableMouse( true)
	frame:SetFrameStrata( 'FULLSCREEN_DIALOG')
	frame:SetScript( 'OnMouseDown', Control_OnMouseDown)
	frame:SetScript( 'OnMouseUp', Control_OnMouseUp)
	
	local logo = frame:CreateTexture( nil, 'ARTWORK')
	logo:SetTexture( 'Interface\\Icons\\Trade_BlackSmithing')
	logo:SetPoint( 'TOPLEFT', frame, 'TOPLEFT', 4, -4)
	logo:SetWidth( 40)
	logo:SetHeight( 40)

	local close = CreateFrame( 'Button', nil, frame)
	close:SetNormalTexture( 'Interface\\Buttons\\UI-Panel-MinimizeButton-Up')
	close:SetPushedTexture( 'Interface\\Buttons\\UI-Panel-MinimizeButton-Down')
	close:SetHighlightTexture( 'Interface\\Buttons\\UI-Panel-MinimizeButton-Highlight')
	close:GetNormalTexture():SetTexCoord( 0.25, 0.75, 0.25, 0.75)
	close:GetPushedTexture():SetTexCoord( 0.25, 0.75, 0.25, 0.75)
	close:GetHighlightTexture():SetTexCoord( 0.25, 0.75, 0.25, 0.75)
	close:GetHighlightTexture():SetBlendMode( 'ADD')
	close:SetPoint( 'TOPRIGHT', frame, 'TOPRIGHT', -4, -4)
	close:SetWidth( 20)
	close:SetHeight( 20)
	close:SetScript( 'OnClick', Control_OnClose)

	local link = CreateFrame( 'Button', nil, frame)
	link:SetNormalTexture( 'Interface\\TradeSkillFrame\\UI-TradeSkill-LinkButton')
	link:SetHighlightTexture( 'Interface\\TradeSkillFrame\\UI-TradeSkill-LinkButton')
	link:GetNormalTexture():SetTexCoord( 0, 0.84375, 0, 0.5)
	link:GetHighlightTexture():SetTexCoord( 0, 0.84375, 0.5, 1)
	link:GetHighlightTexture():SetBlendMode( 'ADD')
	link:SetPoint( 'TOPRIGHT', close, 'TOPLEFT', 0, 0)
	link:SetWidth( 27)
	link:SetHeight( 20)
	link:SetScript( 'OnClick', Control_OnLink)
	link:SetScript( 'OnEnter', Control_OnEnter)
	link:SetScript( 'OnLeave', Control_OnLeave)
	link:Hide()
	link.tooltip = LINK_TRADESKILL_TOOLTIP

	local lower = CreateFrame( 'Button', nil, frame)
	lower:SetNormalTexture( 'Interface\\Minimap\\UI-Minimap-ZoomInButton-Up')
--	lower:SetHighlightTexture( 'Interface\\TimeManager\\ResetButton')
--	lower:GetNormalTexture():SetTexCoord( 0, 0.84375, 0, 0.5)
--	lower:GetHighlightTexture():SetTexCoord( 0, 0.84375, 0.5, 1)
--	lower:GetHighlightTexture():SetBlendMode( 'ADD')
	lower:SetPoint( 'TOPRIGHT', link, 'TOPLEFT', 0, 0)
	lower:SetWidth( 20)
	lower:SetHeight( 20)
	lower:SetScript( 'OnClick', Control_OnLower)
	lower:SetScript( 'OnEnter', Control_OnEnter)
	lower:SetScript( 'OnLeave', Control_OnLeave)
	lower:Hide()
	lower.tooltip = L.LowerSpells

	local char = CreateFrame( 'Button', nil, frame)
	char.text = char:CreateFontString( nil, 'ARTWORK')
	char.text:SetFontObject( GameFontHighlightSmall)
	char.text:SetFont( STANDARD_TEXT_FONT, 20)
	char.text:SetJustifyH( 'LEFT')
	char.text:SetAllPoints( char)
--	char:SetPoint( 'TOPRIGHT', lower, 'TOPLEFT', -4, 0)
	char:SetPoint( 'TOPLEFT', logo, 'TOPRIGHT', 4, 0)
	char:SetPoint( 'TOPRIGHT', lower, 'TOPLEFT', -4, 0)
	char:SetHeight( 20)
	char:SetScript( 'OnClick', Control_OnSwitch)
	char:SetScript( 'OnEnter', Control_OnEnter)
	char:SetScript( 'OnLeave', Control_OnLeave)
	char.tooltip = L.SwitchCharDesc

	if ARL and ARL.scan_button then
		local arl = ARL.scan_button
		arl:SetParent( frame)
		arl:SetText( L.ARL)
		arl:ClearAllPoints()
		arl:SetPoint( 'TOPRIGHT', close, 'BOTTOMRIGHT', 0, 0)
		arl:SetWidth( 27)
		arl:SetHeight( 20)
		arl:SetFrameLevel( frame:GetFrameLevel() + 1)
		arl:SetFrameStrata( frame:GetFrameStrata())
		arl:Enable()
	end

	local save = CreateFrame( 'Button', nil, frame)
	save:SetNormalTexture( 'Interface\\Icons\\INV_Misc_Bag_28_Halloween')
	save:SetPoint( 'TOPRIGHT', close, 'BOTTOMRIGHT', -27, 0)
	save:SetWidth( 20)
	save:SetHeight( 20)
	save:SetScript( 'OnClick', Control_OnSave)
	save:SetScript( 'OnEnter', Control_OnEnter)
	save:SetScript( 'OnLeave', Control_OnLeave)
	save:Hide()
	save.tooltip = L.SaveChar

	local select = CreateFrame( 'Button', nil, frame)
	select.text = select:CreateFontString( nil, 'ARTWORK')
	select.text:SetFontObject( GameFontHighlightSmall)
	select.text:SetFont( STANDARD_TEXT_FONT, UIDROPDOWNMENU_DEFAULT_TEXT_HEIGHT)
	select.text:SetJustifyH( 'LEFT')
	select.text:SetAllPoints( select)
	select:SetPoint( 'RIGHT', save, 'LEFT')
--	select:SetPoint( 'TOPRIGHT', char, 'BOTTOMRIGHT')
	select:SetPoint( 'TOPLEFT', char, 'BOTTOMLEFT')
	select:SetHeight( 20)
	select:SetScript( 'OnClick', Control_OnSelect)
	select:SetScript( 'OnEnter', Control_OnEnter)
	select:SetScript( 'OnLeave', Control_OnLeave)
	select.tooltip = L.SwitchSelect
	
	local tabs = {}
	for i = 1,6 do
		local tab = CreateFrame( 'Button', nil, frame)
		tab.index = i
		if i == 1 then
			tab:SetPoint( 'BOTTOMRIGHT', frame, 'TOPRIGHT', -10, 0)
		else
			tab:SetPoint( 'BOTTOMRIGHT', tabs[i-1], 'BOTTOMLEFT', -3, 0)
		end
		tab:SetWidth( 25)
		tab:SetHeight( 25)
		tab:SetScript( 'OnClick', Control_OnTabClick)
		tab:SetScript( 'OnEnter', Control_OnEnter)
		tab:SetScript( 'OnLeave', Control_OnLeave)
		tab:Hide()
		tabs[i] = tab
	end

	local widget = {
		num    = id,
		frame  = frame,
		logo   = logo,
		close  = close,
		link   = link,
		lower  = lower,
		char   = char,
		save   = save,
		select = select,
		tabs   = tabs,
		type   = Type
	}
	widget.minWidth = 4 + logo:GetWidth() + 4 + 4 + lower:GetWidth() + link:GetWidth() + close:GetWidth() + 4
	-- [_logo_char_lower.link.close_]

	for method,func in pairs(methods) do
		widget[method] = func
	end
	frame.obj  = widget
	close.obj  = widget
	link.obj   = widget
	lower.obj  = widget
	char.obj   = widget
	save.obj   = widget
	select.obj = widget
	for _,tab in pairs( tabs) do
		tab.obj = widget
	end

	return AceGUI:RegisterAsWidget( widget)
end

AceGUI:RegisterWidgetType( Type, Constructor, Version)
