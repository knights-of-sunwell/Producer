## Interface: 30300
## Title: Producer [|cffeda55fPosition|r]
## Notes: Producer sort by position
## Notes-deDE: Producer sortiert nach Position
## Title-ruRU: Producer [|cffeda55fПозиция|r]
## Notes-ruRU: Producer: сортировка по позиции
## Author: Grayal
## Dependencies: Producer
## OptionalDeps: Ace3, LibPeriodicTable-3.1
## X-Email: grayal@gmx.de
## X-Category: Tradeskill
## X-CompatibleLocales: enUS, enGB, deDE, ruRU, zhCN
## X-Compatible-With: 40000
## Version: 1.4
## X-Curse-Packaged-Version: r194
## X-Curse-Project-Name: Producer
## X-Curse-Project-ID: producer
## X-Curse-Repository-ID: wow/producer/mainline

Position.lua
