--[[
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:
	
Free Software Foundation, I.,
51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.
--]]

local AL = LibStub('AceLocale-3.0')

do
	local L = AL:NewLocale( 'ProducerEffect', 'enUS', true)
	L["Agility"] = true
	L["Armor"] = true
	L["Attack Power"] = true
	L["Block Rating"] = true
	L["Critical Strike Rating"] = true
	L["Damage"] = true
	L["Dodge Rating"] = true
	L["Expertise Rating"] = true
	L["Haste Rating"] = true
	L["Health"] = true
	L["Hit Rating"] = true
	L["Intellect"] = true
	L["Jewelcrafting"] = true
	L["Mana"] = true
	L["Mana per 5s"] = true
	L["Mastery Rating"] = true
	L["Miscellaneous"] = true
	L["Parry Rating"] = true
	L["Pets"] = true
	L["Resilience Rating"] = true
	L["Resistance"] = true
	L["Speed"] = true
	L["Spell Penetration"] = true
	L["Spell Power"] = true
	L["Spirit"] = true
	L["Stamina"] = true
	L["Stamina & Spirit"] = true
	L["Strength"] = true
	L["Effect"] = true
end

do
	local L = AL:NewLocale( 'ProducerEffect', 'deDE')
	if L then
		L["Agility"] = "Beweglichkeit"
		L["Armor"] = "Rüstung"
		L["Attack Power"] = "Angriffkraft"
		L["Block Rating"] = "Blockwertung"
		L["Critical Strike Rating"] = "Kritische Trefferwertung"
		L["Damage"] = "Schaden"
		L["Dodge Rating"] = "Ausweichwertung"
		L["Expertise Rating"] = "Waffenkundewertung"
		L["Haste Rating"] = "Tempowertung"
		L["Health"] = "Gesundheit"
		L["Hit Rating"] = "Trefferwertung"
		L["Intellect"] = "Intelligenz"
		L["Jewelcrafting"] = "Juwelenschleifen"
		L["Mana"] = "Mana"
		L["Mana per 5s"] = "Mana alle 5s"
		L["Miscellaneous"] = "Verschiedenes"
		L["Parry Rating"] = "Parierwertung"
		L["Pets"] = "Begleiter"
		L["Resilience Rating"] = "Abhärtungswertung"
		L["Resistance"] = "Widerstand"
		L["Speed"] = "Tempo"
		L["Spell Penetration"] = "Zauberdurchschlag"
		L["Spell Power"] = "Zaubermacht"
		L["Spirit"] = "Willenskraft"
		L["Stamina"] = "Ausdauer"
		L["Stamina & Spirit"] = "Ausdauer & Willenskraft"
		L["Strength"] = "Stärke"
		L["Effect"] = "Auswirkungen"
	end
end

do
	local L = AL:NewLocale( 'ProducerEffect', 'frFR')
	if L then
		L["Agility"] = "Agilité"
		L["Armor"] = "Armure"
		L["Attack Power"] = "Puissance d'attaque"
		L["Block Rating"] = "Score de blocage"
		L["Critical Strike Rating"] = "Score de coup critique"
		L["Damage"] = "Dégâts"
		L["Dodge Rating"] = "Score d'esquive"
		L["Expertise Rating"] = "Score d'expertise"
		L["Haste Rating"] = "Score de hâte"
		L["Health"] = "Vie"
		L["Hit Rating"] = "Score de toucher"
		L["Intellect"] = "Intelligence"
		L["Jewelcrafting"] = "Joaillerie"
		L["Mana"] = "Maná"
		L["Mana per 5s"] = "Mana par 5s"
		L["Miscellaneous"] = "Divers"
		L["Parry Rating"] = "Score de parade"
		L["Pets"] = "Familiers"
		L["Resilience Rating"] = "Score de résilience"
		L["Resistance"] = "Résistance"
		L["Speed"] = "Vitesse"
		L["Spell Penetration"] = "Pénétration des sorts"
		L["Spell Power"] = "Puissance des sorts"
		L["Spirit"] = "Esprit"
		L["Stamina"] = "Endurance"
		L["Stamina & Spirit"] = "Endurance & Esprit"
		L["Strength"] = "Force"
		L["Effect"] = "Effet"
	end
end

do
	local L = AL:NewLocale( 'ProducerEffect', 'esES')
	if L then
		L["Agility"] = "Agilidad"
		L["Attack Power"] = "Poder de ataque"
		L["Block Rating"] = "índice de bloqueo"
		L["Dodge Rating"] = "índice de esquivar"
		L["Intellect"] = "Intelecto"
		L["Mana"] = "Maná"
		L["Spirit"] = "Espíritu"
		L["Stamina"] = "Aguante"
		L["Stamina & Spirit"] = "Aguante & Espíritu"
		L["Strength"] = "Fortaleza"
	end
end

do
	local L = AL:NewLocale( 'ProducerEffect', 'esMX')
	if L then
	end
end

do
	local L = AL:NewLocale( 'ProducerEffect', 'ruRU')
	if L then
		L["Agility"] = "Ловкость"
		L["Armor"] = "Броня"
		L["Attack Power"] = "Сила атаки"
		L["Block Rating"] = "Рейтинг блока"
		L["Critical Strike Rating"] = "Рейтинг критического удара"
		L["Damage"] = "Урон"
		L["Dodge Rating"] = "Рейтинг уклонения"
		L["Expertise Rating"] = "Рейтмнг мастерства"
		L["Haste Rating"] = "Рейтинг скорости"
		L["Health"] = "Здоровье"
		L["Hit Rating"] = "Рейтинг меткости"
		L["Intellect"] = "Интеллект"
		L["Jewelcrafting"] = "Ювелирное дело"
		L["Mana"] = "Мана"
		L["Mana per 5s"] = "Маны раз в 5с"
		L["Miscellaneous"] = "Разное"
		L["Parry Rating"] = "Рейтинг парирования"
		L["Pets"] = "Питомцы"
		L["Resilience Rating"] = "Рейтинг устойчивости"
		L["Resistance"] = "Устойчивость"
		L["Speed"] = "Скорость"
		L["Spell Penetration"] = "Проникновение заклинаний"
		L["Spell Power"] = "Сила заклинаний"
		L["Spirit"] = "Дух"
		L["Stamina"] = "Выносливость"
		L["Stamina & Spirit"] = "Выносливость и Дух"
		L["Strength"] = "Сила"
		L["Effect"] = "Действие"
	end
end

do
	local L = AL:NewLocale( 'ProducerEffect', 'koKR')
	if L then
	end
end

do
	local L = AL:NewLocale( 'ProducerEffect', 'zhCN')
	if L then
		L["Agility"] = "敏捷"
		L["Armor"] = "护甲"
		L["Attack Power"] = "攻击强度"
		L["Block Rating"] = "格挡等级"
		L["Critical Strike Rating"] = "爆击等级"
		L["Damage"] = "伤害"
		L["Dodge Rating"] = "躲闪等级"
		L["Expertise Rating"] = "精准等级"
		L["Haste Rating"] = "急速等级"
		L["Health"] = "生命"
		L["Hit Rating"] = "命中等级"
		L["Intellect"] = "智力"
		L["Jewelcrafting"] = "珠宝加工"
		L["Mana"] = "法力"
		L["Mana per 5s"] = "每5秒回复法力"
		L["Miscellaneous"] = "其它"
		L["Parry Rating"] = "格挡等级"
		L["Pets"] = "宠物"
		L["Resilience Rating"] = "魔法抗性"
		L["Resistance"] = "抗性"
		L["Speed"] = "速度"
		L["Spell Penetration"] = "法术穿透"
		L["Spell Power"] = "法术强度"
		L["Spirit"] = "精神"
		L["Stamina"] = "耐力"
		L["Stamina & Spirit"] = "耐力和精神"
		L["Strength"] = "力量"
		L["Effect"] = "效果"
	end
end

do
	local L = AL:NewLocale( 'ProducerEffect', 'zhTW')
	if L then
	end
end

local L = AL:GetLocale( 'ProducerEffect')
local Producer = LibStub( 'AceAddon-3.0'):GetAddon( 'Producer')
local mod = Producer:NewModule( L["Effect"])

-- Producer.Effect.Cooking
mod:RegisterSelection( 'Cooking', function()
	mod:AddData( 'Cooking', L['Agility'],                        '-88046,-88042,-57441,-33293,-33288,-18240')
	mod:AddData( 'Cooking', L['Attack Power'],                   '-66037,-64054,-62045,-58065,-57423,-46684,-45567,-45563,-45555,-45554,-45549,-33284')
	mod:AddData( 'Cooking', L['Critical Strike Rating'],         '-88028,-88003,-57436,-45571,-45565,-45557,-45551,-45547,-43707')
	mod:AddData( 'Cooking', L['Expertise Rating'],               '-88024,-88014,-57434')
	mod:AddData( 'Cooking', L['Haste Rating'],                   '-88012,-88004,-66034,-62051,-45570,-45569,-45558,-45552,-44438')
	mod:AddData( 'Cooking', L['Health'],                         '-93741,-88018,-88006,-57443,-57438,-57421,-42296,-37836,-33290,-20916,-20626,-18247,-18245,-18244,-18241,-18238,-8607,-7828,-7827,-7755,-7754,-7753,-7752,-7751,-6501,-6417,-6413,-2548,-2543,-2540,-2538')
	mod:AddData( 'Cooking', L['Hit Rating'],                     '-88037,-88020,-66038,-62350,-62050,-57437,-43765')
	mod:AddData( 'Cooking', L['Intellect'],                      '-88039,-88033,-22761')
	mod:AddData( 'Cooking', L['Mana'],                           '-88045,-88044,-64358,-58528,-58527,-53056,-45562,-45561,-45560,-42305,-13028,-2545')
	mod:AddData( 'Cooking', L['Mana per 5s'],                    '-33292,-25954,-25704,-18243')
	mod:AddData( 'Cooking', L['Miscellaneous'],                  '-88017,-88015,-88013,-58525,-58523,-58521,-58512,-57435,-45695,-43779,-43761,-43758,-15906,-9513,-8238')
	mod:AddData( 'Cooking', L['Pets'],                           '-57440,-43772,-33285')
	mod:AddData( 'Cooking', L['Spell Power'],                    '-66035,-62049,-46688,-45568,-45564,-45556,-45550,-38868,-33295,-33294,-33286')
	mod:AddData( 'Cooking', L['Stamina & Spirit'],               '-88047,-88036,-88035,-88034,-88031,-88030,-88025,-88022,-88019,-88016,-88011,-66036,-65454,-62044,-57439,-57433,-45566,-45559,-45553,-45022,-42302,-38867,-36210,-33296,-33291,-33289,-33279,-33278,-33277,-33276,-28267,-25659,-24418,-22480,-21175,-21144,-21143,-18246,-18242,-18239,-15935,-15933,-15915,-15910,-15865,-15863,-15861,-15856,-15855,-15853,-8604,-7213,-6500,-6499,-6419,-6418,-6416,-6415,-6414,-6412,-4094,-3400,-3399,-3398,-3397,-3377,-3376,-3373,-3372,-3371,-3370,-2795,-2549,-2547,-2546,-2544,-2542,-2541,-2539')
	mod:AddData( 'Cooking', L['Strength'],                       '-88021,-88005,-57442,-33287,-24801')
end)

-- Producer.Effect.Enchanting
mod:RegisterSelection( 'Enchanting', function()
	mod:AddData( 'Enchanting', L['Agility'],                     '-95471,-74252,-74216,-74213,-60663,-44633,-44631,-44589,-44529,-44500,-42620,-34007,-34004,-27984,-27977,-27951,-27837,-25083,-25080,-23800,-20023,-20012,-13935,-13882,-13815,-13637,-13419,-7867,-7779')
	mod:AddData( 'Enchanting', L['Armor'],                       '-74250,-74234,-74207,-74200,-74191,-60692,-47900,-47672,-44623,-44492,-33990,-27961,-27960,-27958,-27957,-20028,-20026,-20025,-20015,-13941,-13917,-13858,-13746,-13700,-13663,-13640,-13635,-13626,-13607,-13538,-13464,-13421,-7857,-7776,-7771,-7748,-7443,-7426,-7420')
	mod:AddData( 'Enchanting', L['Attack Power'],                '-74246,-60763,-60707,-60691,-60668,-60621,-60616,-60606,-59621,-44645,-44630,-44595,-44575,-44513,-34002,-33996,-27971')
	mod:AddData( 'Enchanting', L['Block Rating'],                '-74226')
	mod:AddData( 'Enchanting', L['Critical Strike Rating'],      '-74248,-74247,-74230,-74201,-42974,-33993,-25129,-15596')
	mod:AddData( 'Enchanting', L['Damage'],                      '-74211,-74197,-64579,-46578,-44621,-44524,-27967,-27920,-20033,-20031,-20030,-13943,-13937,-13915,-13898,-13695,-13693,-13655,-13653,-13529,-13503,-7788,-7786,-7745')
	mod:AddData( 'Enchanting', L['Dodge Rating'],                '-74244,-74229,-47766,-47051,-46594,-44591,-44489,-27944,-27906,-25086,-13931,-13646,-7428')
	mod:AddData( 'Enchanting', L['Expertise Rating'],            '-74239,-74220,-44598,-44484')
	mod:AddData( 'Enchanting', L['Haste Rating'],                '-74256,-74223,-74199,-74198,-74193,-60609,-59625,-47898,-13948')
	mod:AddData( 'Enchanting', L['Health'],                      '-74195,-44584,-44576,-28004,-25072,-20032,-20016,-7418')
	mod:AddData( 'Enchanting', L['Hit Rating'],                  '-74236,-74232,-63746,-60623,-59619,-44488,-33994,-27954')
	mod:AddData( 'Enchanting', L['Intellect'],                   '-74242,-74240,-74235,-74217,-74202,-60653,-44555,-34001,-27968,-27945,-23804,-20036,-20008,-13822,-13622,-7793')
	mod:AddData( 'Enchanting', L['Mana'],                        '-44509,-33991,-28003,-27948,-27913,-23801')
	mod:AddData( 'Enchanting', L['Mastery Rating'],              '-74255,-74253,-74238,-74132')
	mod:AddData( 'Enchanting', L['Miscellaneous'],               '-93843,-93841,-92370,-71692,-69412,-60619,-45765,-44616,-44506,-44383,-42615,-42613,-32667,-32665,-32664,-28022,-28016,-27927,-27905,-25130,-25127,-25125,-25084,-20051,-17181,-17180,-14810,-14809,-14807,-14293,-13868,-13841,-13702,-13698,-13628,-13620,-13617,-13612,-7795,-7421')
	mod:AddData( 'Enchanting', L['Parry Rating'],                '-64441,-44625,-27946,-13689')
	mod:AddData( 'Enchanting', L['Resilience Rating'],           '-74214,-44588,-33992')
	mod:AddData( 'Enchanting', L['Resistance'],                  '-44596,-44590,-44556,-44494,-44483,-34006,-34005,-28028,-28027,-27962,-27947,-25082,-25081,-20014,-13933,-13794,-13657,-13522,-7861,-7454')
	mod:AddData( 'Enchanting', L['Speed'],                       '-20029,-13947,-13890')
	mod:AddData( 'Enchanting', L['Spell Penetration'],           '-74192,-44582,-34003')
	mod:AddData( 'Enchanting', L['Spell Power'],                 '-62959,-62948,-60767,-60714,-44636,-44635,-44629,-44592,-34010,-33999,-33997,-28019,-27982,-27981,-27975,-27926,-27924,-27917,-27911,-25128,-25126,-25124,-25079,-25078,-25074,-25073,-23802,-22750,-22749,-21931')
	mod:AddData( 'Enchanting', L['Spirit'],                      '-74237,-74231,-74225,-47899,-44593,-44510,-44508,-23803,-20035,-20024,-20009,-13905,-13846,-13687,-13659,-13642,-13485,-13380,-7859,-7766')
	mod:AddData( 'Enchanting', L['Stamina'],                     '-74251,-74218,-74189,-62256,-59636,-47901,-44528,-34009,-34008,-27950,-27914,-20020,-20017,-20011,-13945,-13836,-13817,-13648,-13644,-13631,-13501,-13378,-7863,-7457')
	mod:AddData( 'Enchanting', L['Strength'],                    '-74254,-74215,-74212,-33995,-27972,-27899,-23799,-20034,-20013,-20010,-13939,-13887,-13661,-13536,-7782')
end)

-- Producer.Effect.Jewelcrafting
mod:RegisterSelection( 'Jewelcrafting', function()
	mod:AddData( 'Jewelcrafting', L['Agility'],                  '-95755,-73397,-73371,-73368,-73365,-73361,-73357,-73351,-73336,-73268,-73265,-73262,-73258,-73247,-73241,-73223,-66587,-66585,-66584,-66573,-66568,-66557,-66448,-56052,-55400,-53991,-53988,-53981,-53980,-53963,-53945,-53880,-53879,-53877,-53861,-53860,-53832,-42589,-39961,-39738,-39736,-39728,-39706,-39471,-39467,-34590,-31109,-31103,-31085,-28933,-28914')
	mod:AddData( 'Jewelcrafting', L['Armor'],                    '-73468,-55401')
	mod:AddData( 'Jewelcrafting', L['Critical Strike Rating'],   '-73475,-73474,-73472,-73465,-73406,-73377,-73366,-73364,-73346,-73274,-73263,-73260,-73232,-66569,-66567,-66502,-66441,-66435,-66431,-56085,-55405,-55402,-55394,-55393,-55390,-55389,-55388,-54012,-54003,-53996,-53984,-53975,-53957,-53932,-53922,-53882,-53872,-53870,-53845,-44794,-42592,-42591,-39742,-39740,-39734,-39733,-39720,-32874,-32873,-32871,-31113,-31111,-31107,-31106,-31097,-28944,-28917,-28916,-28915,-28910')
	mod:AddData( 'Jewelcrafting', L['Dodge Rating'],             '-73405,-73376,-73375,-73362,-73345,-73273,-73272,-73259,-73231,-66586,-66581,-66579,-66452,-66429,-66338,-56055,-55398,-54023,-53998,-53997,-53993,-53977,-53948,-53918,-53917,-53893,-53891,-53874,-53843,-46803,-46597,-42593,-39739,-39713,-32868,-31090,-28947,-28918')
	mod:AddData( 'Jewelcrafting', L['Expertise Rating'],         '-73400,-73374,-73360,-73354,-73339,-73271,-73250,-73244,-73226,-66576,-66561,-66450,-56081,-54017,-53994,-53974,-53951,-53892,-53871')
	mod:AddData( 'Jewelcrafting', L['Haste Rating'],             '-73408,-73381,-73380,-73369,-73367,-73348,-73278,-73277,-73266,-73264,-73234,-66583,-66574,-66506,-66443,-66442,-66439,-66434,-56083,-55404,-54019,-54014,-54011,-54009,-54001,-53987,-53961,-53933,-53925,-53923,-53920,-53885,-53876,-53856,-47056,-47055,-47053,-46405,-46403,-31108,-28912')
	mod:AddData( 'Jewelcrafting', L['Hit Rating'],               '-73404,-73384,-73378,-73359,-73358,-73356,-73344,-73281,-73275,-73249,-73248,-73246,-73230,-66572,-66570,-66501,-56084,-53985,-53976,-53958,-53883,-53873,-53854,-39737,-39721,-39470,-39466,-31098,-28948')
	mod:AddData( 'Jewelcrafting', L['Intellect'],                '-73476,-73470,-73466,-73399,-73355,-73353,-73338,-73245,-73243,-73225,-66562,-66556,-66446,-66432,-56053,-55403,-55397,-55396,-55392,-55387,-55386,-53968,-53966,-53965,-53946,-53921,-53894,-53865,-53852,-46601,-42588,-42558,-41429,-41420,-39741,-39731,-39711,-32870,-32867,-31112,-31104,-31088,-28927,-28925,-28903')
	mod:AddData( 'Jewelcrafting', L['Jewelcrafting'],            '-73478,-62941,-62242,-55395,-47280,-39963,-32872,-32869,-26880,-25615,-25278,-25255')
	mod:AddData( 'Jewelcrafting', L['Mastery Rating'],           '-73464,-73409,-73383,-73382,-73373,-73372,-73370,-73349,-73280,-73279,-73270,-73269,-73267,-73239')
	mod:AddData( 'Jewelcrafting', L['Miscellaneous'],            '-73643,-73642,-73641,-73639,-73627,-73626,-73625,-73624,-73623,-73622,-73621,-73620,-73521,-73520,-73506,-73505,-73504,-73503,-73502,-73498,-73497,-73496,-73495,-73494,-68253,-64728,-64727,-64726,-64725,-63743,-59759,-58954,-58507,-58492,-58150,-58149,-58148,-58147,-58146,-58145,-58144,-58143,-58142,-58141,-56531,-56530,-56501,-56500,-56499,-56498,-56497,-56496,-56208,-56206,-56205,-56203,-56202,-56201,-56199,-56197,-56196,-56195,-56194,-56193,-46779,-46778,-46777,-46776,-46775,-46127,-46126,-46125,-46124,-46123,-46122,-41418,-41415,-41414,-40514,-38504,-38503,-38175,-38068,-37855,-37818,-36526,-36525,-36524,-36523,-34961,-34960,-34959,-34955,-32809,-32808,-32807,-32801,-32259,-32179,-32178,-31083,-31082,-31081,-31080,-31079,-31078,-31077,-31076,-31072,-31071,-31070,-31068,-31067,-31066,-31065,-31064,-31063,-31062,-31061,-31060,-31058,-31057,-31056,-31055,-31054,-31053,-31052,-31051,-31050,-31049,-31048,-26928,-26927,-26926,-26925,-26920,-26918,-26916,-26915,-26914,-26912,-26911,-26910,-26909,-26908,-26907,-26906,-26903,-26902,-26900,-26897,-26896,-26887,-26885,-26883,-26882,-26881,-26878,-26876,-26875,-26874,-26873,-26872,-25622,-25621,-25620,-25619,-25618,-25617,-25613,-25612,-25610,-25498,-25493,-25490,-25339,-25323,-25321,-25320,-25318,-25317,-25305,-25287,-25284,-25283,-25280')
	mod:AddData( 'Jewelcrafting', L['Parry Rating'],             '-73398,-73352,-73337,-73242,-73224,-66560,-66453,-56056,-53972,-53949,-53869,-53844,-39714,-31091')
	mod:AddData( 'Jewelcrafting', L['Resilience Rating'],        '-95756,-95754,-73407,-73379,-73347,-73276,-73233,-66582,-66571,-66505,-66445,-66428,-56079,-54005,-54000,-53986,-53978,-53960,-53924,-53919,-53884,-53875,-53857,-47054,-43493,-39724,-31101')
	mod:AddData( 'Jewelcrafting', L['Spell Penetration'],        '-73403,-73343,-73229,-66499,-56088,-53955,-53943,-39718,-31095,-28955')
	mod:AddData( 'Jewelcrafting', L['Spirit'],                   '-73473,-73471,-73469,-73467,-73402,-73341,-73228,-66498,-56087,-55407,-53954,-53941,-39716,-31149,-28953')
	mod:AddData( 'Jewelcrafting', L['Stamina'],                  '-73401,-73340,-73227,-66497,-56086,-55399,-55384,-53952,-53934,-42590,-39715,-32866,-31092,-28950')
	mod:AddData( 'Jewelcrafting', L['Strength'],                 '-73396,-73350,-73335,-73240,-73222,-66554,-66447,-56049,-53962,-53859,-53831,-53830,-39727,-39705,-31102,-31084,-28936,-28905')
end)
