## Interface: 30300
## Title: Producer [|cffeda55fEffect|r]
## Notes: Producer sort by effect
## Title-ruRU: Producer [|cffeda55fДействие|r]
## Notes-ruRU: Producer: сортировка по действию
## Notes-deDE: Producer sortiert nach Auswirkungen
## Author: Grayal
## Dependencies: Producer
## OptionalDeps: Ace3, LibPeriodicTable-3.1
## X-Email: grayal@gmx.de
## X-Category: Tradeskill
## X-CompatibleLocales: enUS, enGB, deDE, ruRU, zhCN
## X-Compatible-With: 40000
## Version: 1.4
## X-Curse-Packaged-Version: r194
## X-Curse-Project-Name: Producer
## X-Curse-Project-ID: producer
## X-Curse-Repository-ID: wow/producer/mainline

Effect.lua
